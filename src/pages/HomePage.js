import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import {
  MDBEdgeHeader,
  MDBContainer,
  MDBCol,
  MDBRow,

} from 'mdbreact';
import SectionContainer from '../components/sectionContainer';
import './HomePage.css';

 class HomePage extends React.Component {
  
  scrollToTop = () => window.scrollTo(0, 0);
  render() {


    return (
      <Router>
        <MDBEdgeHeader color='indigo darken-3' className='sectionPage align-items-stretch' style={{ height: '160px' }} />      
        <SectionContainer header='Slim' flexCenter>
          <div className='mt-5 mb-5'>           
            <MDBContainer>
              <MDBRow>
                <MDBCol md='12' className='mt-4'>                 
                  <MDBRow id='categories' className='text-black mt-5'>
                           <h4>Have to be defined.</h4>                                         
                  </MDBRow>
                </MDBCol>
              </MDBRow>
            </MDBContainer>
          </div>
        </SectionContainer>
      </Router>
    );
  }
}

export default HomePage;
