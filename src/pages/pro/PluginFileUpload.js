import React from "react";
import MDBFileupload from "mdb-react-fileupload";

const App = () => {
  return (
    <MDBFileupload containerHeight={400} defaultFileSrc='https://mdbootstrap.com/img/Photos/Others/images/89.jpg' />
  );
};

export default App;