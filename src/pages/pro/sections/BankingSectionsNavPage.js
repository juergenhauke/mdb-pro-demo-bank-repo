import React from 'react';
import { MDBEdgeHeader, MDBContainer, MDBRow, MDBCol, MDBJumbotron, MDBIcon, MDBAnimation, MDBSmoothScroll } from 'mdbreact';
import MenuLink from '../../../components/menuLink';
import './BankingSectionNavPage.css';


const SectionsNavPage = () => {
  return (
    <>
      <MDBEdgeHeader color='morpheus-den-gradient' className='banking-sectionPage' />    
      <MDBAnimation type='zoomIn' duration='500ms' id="page-top">
        <MDBContainer>
        <MDBSmoothScroll
            size='lg'
            fixed
            floating
            className='morpheus-den-gradient'
            to='page-top'
            smooth
          >
            <MDBIcon icon='angle-up' />
          </MDBSmoothScroll>
          <MDBRow>
            <MDBCol md='8' className='mt-3 mx-auto'>
              <MDBJumbotron className='morpheus-den-gradient'>
                <h1 className='text-center white-text'>
                  <MDBIcon icon='university' className='white-text mr-2' />
                  Banking-Overview
                </h1>
                <ul className='list-unstyled example-components-list'>
                  <h5 className='white-text'><strong className='white-text'>Sections</strong></h5>
                  <MenuLink to='/banking-landing-page' title='Intro Banking Landing Page' />
                  <MenuLink to='/banking-register' title='Registration' />
                  <MenuLink to='/banking-login' title='Login' />
                  <MenuLink to='/sections/classicform' title='Create Account' />
                  <MenuLink to='/sections/videobackground' title='Edit Account' />
                  <MenuLink to='/sections/minimalistic-intro' title='Create Daily Deposit Account' />
                  <MenuLink to='/sections/parallax-intro' title='Edit Daily Deposit Account' />
                  <MenuLink to='/sections/call-to-action-intro' title='Create Fixed Deposit Account' />
                  <MenuLink to='/sections/contact' title='Edit Fixed Deposit Account' />
                  <MenuLink to='/banking-overview' title='Account Overview' />
                  <MenuLink to='/sections/ecommerce' title='Support' />
                  <MenuLink to='/banking-help' title='Help' />
                  <MenuLink to='/sections/magazine' title='Chat' />
                  <MenuLink to='/banking-contactform' title='Contact' />
                </ul>
              </MDBJumbotron>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </MDBAnimation>
    </>
  );
}

export default SectionsNavPage;
