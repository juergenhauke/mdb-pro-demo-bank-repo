import React from 'react';
import {
  MDBMask,
  MDBRow,
  MDBCol,
  MDBSticky,
  MDBStickyContent,
  MDBTreeview,
  MDBTreeviewList,
  MDBTreeviewItem,
  MDBView,
  MDBContainer,
  MDBAnimation,
  Link,
  MDBSmoothScroll,
  MDBIcon
} from 'mdbreact';
import './AppPage.css';


class AppPage extends React.Component {
  state = {
    collapsed: false
  };

  handleTogglerClick = () => {
    const { collapsed } = this.state;
    this.setState({
      collapsed: !collapsed
    });
  };

  componentDidMount() {
    document.querySelector('nav').style.height = '65px';
  }

  componentWillUnmount() {
    document.querySelector('nav').style.height = 'auto';
  }

  render() {
    const sidebar = { float: 'left', width: '330px', padding: '10px 0' };
    return (
      <div id='apppage'>
        <MDBSmoothScroll
            size='lg'
            fixed
            floating
            className='morpheus-den-gradient'
            to='page-top'
            smooth
          >
            <MDBIcon icon='angle-up' />
          </MDBSmoothScroll> 
        
        <MDBView id="page-top">
          <MDBMask className='white-text gradient' />
          <MDBContainer
            style={{ height: '100%', width: '100%', paddingTop: '10rem' }}
            className='d-flex justify-content-center white-text align-items-center'
          >
            <MDBRow>
              <MDBCol md='6' className='text-center text-md-left mt-xl-5 mb-5'>
                <MDBAnimation type='fadeInLeft' delay='.3s'>
                  <h1 className='h1-responsive font-weight-bold mt-sm-5'>
                    Make purchases with our app
                  </h1>
                  <div style={sidebar}>

                      <MDBStickyContent style={{ height: '800px' }}>
                        <MDBSticky>
                          {({

                            style

                          }) => {
                            return (
                              <div
                                style={{
                                  ...style
                                }}
                              >

                                <MDBTreeview
                                  theme='animated'
                                  header='Navigation'
                                  className='w-20 morpheus-den-gradient border-top border-bottom z-depth-5 white-text'
                                  getValue={value => console.log(value)}
                                >
                                  <MDBTreeviewList title='Tagesgeld' far open>
                                  <Link to='/banking-overview' target='_parent' ><MDBTreeviewItem icon='eye' title='Auszahlung' far /></Link>
                                  <Link to='/banking-register' target='_parent' ><MDBTreeviewItem icon='eye' title='Geplante Auszahlungen' far /></Link>
                                    <MDBTreeviewItem icon='edit' title='Umsätze anzeigen' far />
                                    <MDBTreeviewList icon='edit' title='Tagesgeld bearbeiten' far open>
                                      <MDBTreeviewItem icon='edit' title='Tagesgeld ändern' far />
                                      <MDBTreeviewItem icon='edit' title='Tagesgeld löschen' opened />
                                      <MDBTreeviewItem icon='edit' title='Tagesgeld anlegen' />
                                    </MDBTreeviewList>
                                  </MDBTreeviewList>
                                  <MDBTreeviewList title='Festgeld' far>
                                    <MDBTreeviewItem title='Umsätze anzeigen' far />
                                    <MDBTreeviewItem title='Prolongation' far />
                                    <MDBTreeviewList icon='edit' title='Tagesgeld bearbeiten' far open>
                                      <MDBTreeviewItem title='Festgeld ändern' far />
                                      <MDBTreeviewItem title='Festgeld löschen' far />
                                      <MDBTreeviewItem title='Festgeld anlegen' far />
                                    </MDBTreeviewList>

                                  </MDBTreeviewList>
                                  <MDBTreeviewList title='Postbox' far>
                                    <MDBTreeviewItem title='Neue Nachrichten' far />
                                    <MDBTreeviewItem title='Gelesene Nachrichten' far />
                                    <MDBTreeviewItem title='Nachrichten schreiben' far />
                                  </MDBTreeviewList>
                                  <MDBTreeviewList title='Service' far>
                                    <MDBTreeviewItem icon='eye' title='Persönliche Daten' />
                                    <MDBTreeviewItem icon='edit' title='Persönliche Daten bearbeiten' far />
                                    <MDBTreeviewItem icon='eye' title='Benutzerkennung und Passwort' />
                                    <MDBTreeviewItem icon='eye' title='Freistellungsauftrag' />
                                    <MDBTreeviewItem icon='eye' title='Einwilligungen' />
                                    <MDBTreeviewItem icon='eye' title='Nichtveranlagungsbescheinigung' />
                                  </MDBTreeviewList>
                                </MDBTreeview>
                              </div>
                            );
                          }}
                        </MDBSticky>
                      </MDBStickyContent>
                    </div>
                </MDBAnimation>
              </MDBCol>

              <MDBCol md='6' xl='5' className='mt-xl-5'>
                <MDBAnimation type='fadeInRight' delay='.3s'>
                  <img
                    src='https://mdbootstrap.com/img/Mockups/Transparent/Small/admin-new.png'
                    alt=''
                    className='img-fluid'
                  />
                </MDBAnimation>
              </MDBCol>
            </MDBRow>
          </MDBContainer>
        </MDBView>

        <MDBContainer>
          <MDBRow className='py-5'>
            <MDBCol md='12' className='text-center'>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </div>
    );
  }
}

export default AppPage;
