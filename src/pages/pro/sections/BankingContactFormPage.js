import React from 'react';
import {
  MDBMask,
  MDBRow,
  MDBCol,
  MDBBtn,
  MDBView,
  MDBIcon,
  MDBContainer,
  MDBCard,
  MDBCardBody,
  MDBInput,
  MDBAnimation,
  MDBSmoothScroll
} from 'mdbreact';
import './BankingContactFormPage.css';

class ContactFormPage extends React.Component {

  render() {

    return (

      <div id='bankingcontactformpage'>
        <MDBSmoothScroll
            size='lg'
            fixed
            floating
            className='morpheus-den-gradient'
            to='page-top'
            smooth
          >
            <MDBIcon icon='angle-up' />
          </MDBSmoothScroll>        
        <MDBView id ='page-top'>
          <MDBMask overlay='white-text' />
          <MDBContainer
            style={{ height: '100%', width: '100%', paddingTop: '10rem' }}
            className='d-flex justify-content-center align-items-center white-text'
          >                      
            <MDBAnimation type='fadeInLeft' delay='.3s' className='morpheus-den-gradient rounded z-depth-5 p-sm-5 mb-5 border'>
            <MDBContainer>                       
            <h2 className='h1-responsive font-weight-bold text-center my-5 white-text'>
              Contact us
            </h2>
            
            <p className='text-center w-responsive mx-auto pb-5 white-text'>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit, error
              amet numquam iure provident voluptate esse quasi, veritatis totam
              voluptas nostrum quisquam eum porro a pariatur veniam.
            </p>
            
            <MDBRow>
              <MDBCol lg='5' className='lg-0 mb-4'>
                <MDBCard>
                  <MDBCardBody>
                    <div className='form-header morpheus-den-gradient rounded'>
                      <h3 className='mt-2'>
                        <MDBIcon icon='envelope' /> Write to us:
                      </h3>
                    </div>
                    <p className='dark-grey-text'>
                      We'll write rarely, but only the best content.
                    </p>
                    <div className='md-form'>
                      <MDBInput
                        icon='user'
                        label='Your name'
                        iconClass='grey-text'
                        type='text'
                        id='form-name'
                      />
                    </div>
                    <div className='md-form'>
                      <MDBInput
                        icon='envelope'
                        label='Your email'
                        iconClass='grey-text'
                        type='text'
                        id='form-email'
                      />
                    </div>
                    <div className='md-form'>
                      <MDBInput
                        icon='tag'
                        label='Subject'
                        iconClass='grey-text'
                        type='text'
                        id='form-subject'
                      />
                    </div>
                    <div className='md-form'>
                      <MDBInput
                        icon='pencil-alt'
                        label='Your Message'
                        iconClass='grey-text'
                        type='textarea'
                        id='form-text'
                      />
                    </div>
                    <div className='text-center'>
                      <MDBBtn className='morpheus-den-gradient'>Submit</MDBBtn>
                    </div>
                  </MDBCardBody>
                </MDBCard>
              </MDBCol>
              <MDBCol lg='7'>
                <div
                  id='map-container'
                  className='rounded z-depth-1-half map-container'
                  style={{ height: '400px' }}
                >
                  <iframe
                    src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d163720.8725362008!2d8.496482157975734!3d50.12112774342282!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47bd096f477096c5%3A0x422435029b0c600!2sFrankfurt%20am%20Main!5e0!3m2!1sde!2sde!4v1585906331289!5m2!1sde!2sde'
                    title='Demo Bank'
                    width='100%'
                    height='100%'
                    frameBorder='0'
                    style={{ border: 0 }}
                    className='rounded'
                  />
                </div>
                <br />
                <MDBRow className='text-center'>
                  <MDBCol md='4'>
                    <MDBBtn tag='a' floating className='morpheus-den-gradient'>
                      <MDBIcon icon='map-marker-alt' />
                    </MDBBtn>
                    <p>Frankfurt, 60306</p>
                    <p className='mb-md-0'>Germany</p>
                  </MDBCol>
                  <MDBCol md='4'>
                    <MDBBtn tag='a' floating color='blue' className='morpheus-den-gradient'>
                      <MDBIcon icon='phone' />
                    </MDBBtn>
                    <p>+ 69 234 567 89</p>
                    <p className='mb-md-0'>Mon - Fri, 8:00-18:00</p>
                  </MDBCol>
                  <MDBCol md='4'>
                    <MDBBtn tag='a' floating color='blue' className='morpheus-den-gradient'>
                      <MDBIcon icon='envelope' />
                    </MDBBtn>
                    <p>info@demo-bank.com</p>
                    <p className='mb-md-0'>support@demo-bank</p>
                  </MDBCol>
                </MDBRow>
              </MDBCol>
            </MDBRow>
          </MDBContainer>

            </MDBAnimation>
            
      
          </MDBContainer>
        </MDBView>

        <MDBContainer>
          <MDBRow className='py-5'>
            <MDBCol md='12' className='text-center'>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </div>
    );
  }
}

export default ContactFormPage;
