import React from 'react';
import {
  MDBMask,
  MDBRow,
  MDBCol,
  MDBBtn,
  MDBView,
  MDBContainer,
  MDBAnimation,
  MDBSmoothScroll,
  MDBIcon
} from 'mdbreact';
import './BankingLandingPage.css';

class AppPage extends React.Component {



  render() {

    return (
      <div id='bankinglandingpage'>
        <MDBSmoothScroll
            size='lg'
            fixed
            floating
            className='morpheus-den-gradient'
            to='page-top'
            smooth
          >
            <MDBIcon icon='angle-up' />
          </MDBSmoothScroll>
        
        <MDBView>
          <MDBMask className='white-text morpheus-den-gradient' style={{opacity: '.9'}} />
          <MDBContainer
            style={{ height: '100%', width: '100%', paddingTop: '8rem' }}
            className='d-flex justify-content-center white-text align-items-center'
            id="page-top"
          >
            <MDBRow>
              <MDBCol md='6' className='text-center text-md-left mt-xl-5 mb-5'>
                <MDBAnimation type='fadeInLeft' delay='.3s' className='morpheus-den-gradient rounded z-depth-5 p-5 border'>
                  <h1 className='h1-responsive font-weight-bold mt-sm-5'>
                    Banking Landing Page
                  </h1>
                  <hr className='hr-light' />
                  <h6 className='mb-4'>
                    If you already have an account please login by clicking the button 'Login' below.<br />
                    If you havent an account already yet, you can sign up for an account by clicking the button 'Register' below.
                  </h6>
                  <MDBBtn color='white' href='/banking-login'>Login</MDBBtn>
                  <MDBBtn outline color='white' href='/banking-register'>
                    Register
                  </MDBBtn>
                </MDBAnimation>
              </MDBCol>

              <MDBCol md='6' xl='5' className='mt-xl-5'>
                <MDBAnimation type='fadeInRight' delay='.3s'>
                  <img
                    src='https://web-complett.de/mdb-react-pro/bank/bank-landing-page.png'
                    alt=''
                    className='img-fluid'
                  />
                </MDBAnimation>
              </MDBCol>
            </MDBRow>
          </MDBContainer>
        </MDBView>

        <MDBContainer>
          <MDBRow className='py-5'>
            <MDBCol md='12' className='text-center'>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </div>
    );
  }
}

export default AppPage;
