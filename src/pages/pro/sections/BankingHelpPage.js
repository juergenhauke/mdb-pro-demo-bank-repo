import React from 'react';
import {
  MDBCollapse,
  MDBMask,
  MDBRow,
  MDBCol,
  MDBCollapseHeader,
  MDBView,
  MDBIcon,
  MDBContainer,
  MDBCard,
  MDBCardBody,
  MDBAnimation,
  MDBSmoothScroll
} from 'mdbreact';
import './BankingHelpPage.css';

class ContactFormPage extends React.Component {
  state = {
    collapseID: 'collapse3'
  };

  toggleCollapse = collapseID => () =>
    this.setState(prevState => ({
      collapseID: prevState.collapseID !== collapseID ? collapseID : ''
    }));



  render() {
    const { collapseID } = this.state;

    return (
      <div id='bankinghelppage'>
        <MDBSmoothScroll
            size='lg'
            fixed
            floating
            className='morpheus-den-gradient'
            to='page-top'
            smooth
          >
            <MDBIcon icon='angle-up' />
          </MDBSmoothScroll> 

        <MDBView id ='page-top'>
          <MDBMask overlay='white-text' />
          <MDBContainer
            style={{ height: '100%', width: '100%', paddingTop: '8rem' }}
            className='d-flex justify-content-center align-items-center white-text'
          >
            
            
            <MDBAnimation type='fadeInLeft' delay='.3s' className='morpheus-den-gradient rounded z-depth-5 p-sm-5 mb-5 border'>
            <MDBContainer>                       
            <h2 className='h1-responsive font-weight-bold text-center my-5 white-text'>
              Online-Hilfe
            </h2>
            
            <p className='text-center w-responsive mx-auto pb-5 white-text'>
             Wir haben für Sie die häufigst gestellten Fragen zusammengestellt und hoffen Ihr Anliegen damit beantworten zu können. Sollten Sier keine hinreichende Auskunft erhalten können Sier gerne an unser Service-Team wenden, das Ihnen sehr gerne weiterhilft. 
            </p>
            
            
          </MDBContainer>
          <MDBContainer header='With icons' className='accordion md-accordion accordion-3'>
          <h2 className='text-center text-uppercase white-text py-4 px-3'>
            Häufig gestellte Fragen
          </h2>

          <hr className='mb-0' />

          <MDBCard>
            <MDBCollapseHeader
              onClick={this.toggleCollapse('collapse10')}
              tag='h3'
              tagClassName='bank-text d-flex justify-content-between align-items-center'
            >
              Was bedeutet 2-Faktor-Authentifizierung?
              <MDBIcon
                icon={collapseID === 'collapse10' ? 'angle-up' : 'angle-down'}
                className='bank-text'
                size='2x'
              />
            </MDBCollapseHeader>
            <MDBCollapse id='collapse10' isOpen={collapseID}>
              <MDBCardBody className='pt-0'>
                <p>
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                  wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                  eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
                  assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                  nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                  farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus
                  labore sustainable VHS.
                </p>
              </MDBCardBody>
            </MDBCollapse>
          </MDBCard>

          <MDBCard>
            <MDBCollapseHeader
              onClick={this.toggleCollapse('collapse11')}
              tag='h3'
              tagClassName='bank-text d-flex justify-content-between align-items-center'
            >
              Wie lange ist mein Passwort gültig?
              <MDBIcon
                icon={collapseID === 'collapse11' ? 'angle-up' : 'angle-down'}
                className='bank-text'
                size='2x'
              />
            </MDBCollapseHeader>
            <MDBCollapse id='collapse11' isOpen={collapseID}>
              <MDBCardBody className='pt-0'>
                <p>
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                  wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                  eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
                  assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                  nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                  farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus
                  labore sustainable VHS.
                </p>
              </MDBCardBody>
            </MDBCollapse>
          </MDBCard>

          <MDBCard>
            <MDBCollapseHeader
              onClick={this.toggleCollapse('collapse12')}
              tag='h3'
              tagClassName='bank-text d-flex justify-content-between align-items-center'
            >
              Wie kann ich ein Konto löschen / auflösen?
              <MDBIcon
                icon={collapseID === 'collapse12' ? 'angle-up' : 'angle-down'}
                className='bank-text'
                size='2x'
              />
            </MDBCollapseHeader>
            <MDBCollapse id='collapse12' isOpen={collapseID}>
              <MDBCardBody className='pt-0'>
                <p>
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                  wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                  eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
                  assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                  nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                  farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus
                  labore sustainable VHS.
                </p>
              </MDBCardBody>
            </MDBCollapse>
          </MDBCard>
        </MDBContainer>

            </MDBAnimation>
            
      
          </MDBContainer>
        </MDBView>

        <MDBContainer>
          <MDBRow className='py-5'>
            <MDBCol md='12' className='text-center'>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </div>
    );
  }
}

export default ContactFormPage;
