import React from 'react';
import {

  MDBMask,
  MDBRow,
  MDBCol,
  MDBIcon,
  MDBBtn,
  MDBView,
  MDBContainer,
  MDBCard,
  MDBCardBody,
  MDBInput,
  MDBAnimation,
  MDBSmoothScroll,
  MDBBreadcrumb,
  MDBBreadcrumbItem
} from 'mdbreact';
import './BankingRegisterPage.css';

class ClassicFormPage extends React.Component {


  render() {

    return (
      <div id='bankregisterpage'>

        <MDBSmoothScroll
          size='lg'
          fixed
          floating
          className='morpheus-den-gradient'
          to='page-top'
          smooth
        >
          <MDBIcon icon='angle-up' />
        </MDBSmoothScroll>

        <MDBView id="page-top">

          <MDBMask className='d-flex justify-content-center align-items-center morpheus-den-gradient' style={{ opacity: '.9' }} />
          <MDBContainer
            style={{paddingTop: '10rem' }}
            className='mt-5 d-flex'
          >
          <MDBRow>
              <MDBAnimation
                type='fadeInTop'
                delay='.3s'
                className='white-text text-center text-md-left col-md-12'>
                <MDBContainer>
                  <MDBBreadcrumb  style={{background:'transparent'}}>
                    <MDBBreadcrumbItem>Home</MDBBreadcrumbItem>
                    <MDBBreadcrumbItem>Library</MDBBreadcrumbItem>
                    <MDBBreadcrumbItem active>Data</MDBBreadcrumbItem>
                  </MDBBreadcrumb>
                </MDBContainer>

              </MDBAnimation>
            </MDBRow>
            </MDBContainer>
          <MDBContainer
            style={{ height: '100%', width: '100%', paddingTop: '0rem' }}
            className='d-flex justify-content-center align-items-center '
          >
            

            <MDBRow>



              <MDBAnimation
                type='fadeInLeft'
                delay='.3s'
                className='white-text text-center text-md-left col-md-6  mb-5'
              >
                <MDBCard id='classic-card' className='rounded z-depth-5 border' style={{ background: 'transparent' }}>
                  <MDBCardBody className="white-text">
                    <h3 className='text-left'>
                      <MDBIcon icon='user' /> Kontoeröffnung:
                      </h3>
                    <hr className='hr-light' />
                    <h6 className='mb-4'>
                      Eröffnen Sie noch heute ein Konto bei Ihrer Demo-Bank und profitieren Sie von unseren extra auf Sie zugeschnittenen Angeboten.
                      Starten Sie indem Sie sich für einen Benutzer Account bei uns über das rechte Formular registrieren.
                </h6>
                    <hr className='hr-light' />
                    <h6 className='mt-2'>Falls Sie schon ein Konto bei uns haben, können Sie sich hier einloggen .</h6>
                    <MDBBtn outline color='white' href='/banking-login'>
                      Zu Ihrem Konto
                </MDBBtn>

                  </MDBCardBody>
                </MDBCard>

              </MDBAnimation>

              <MDBCol md='6' xl='5' className='mb-4'>
                <MDBAnimation type='fadeInRight' delay='.3s'>
                  <MDBCard id='classic-card' className='morpheus-den-gradient rounded z-depth-5 border'>
                    <MDBCardBody className='white-text'>
                      <h3 className='text-center'>
                        <MDBIcon icon='user' /> Registrierung:
                      </h3>
                      <hr className='hr-light' />
                      <MDBInput
                        className='white-text'
                        iconClass='white-text'
                        label='Ihr Name'
                        icon='user'
                      />
                      <MDBInput
                        className='white-text'
                        iconClass='white-text'
                        label='Ihre E-Mail'
                        icon='envelope'
                      />
                      <MDBInput
                        className='white-text'
                        iconClass='white-text'
                        label='Ihr Passwort'
                        icon='lock'
                        type='password'
                      />
                      <MDBInput
                        className='white-text'
                        iconClass='white-text'
                        label='Passwort wiederholen'
                        icon='lock'
                        type='password'
                      />
                      <div className='text-center mt-4 black-text'>
                        <MDBBtn outline color='white'>Registrieren</MDBBtn>
                      </div>
                    </MDBCardBody>
                  </MDBCard>
                </MDBAnimation>
              </MDBCol>
            </MDBRow>
          </MDBContainer>
        </MDBView>

        <MDBContainer>
          <MDBRow className='py-5'>
            <MDBCol md='12' className='text-center'>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </div>
    );
  }
}

export default ClassicFormPage;
