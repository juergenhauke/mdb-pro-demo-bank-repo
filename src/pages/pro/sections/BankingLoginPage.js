import React from 'react';
import { useState } from "react"; 
import { useSpeechSynthesis } from "react-speech-kit";
import {

  MDBMask,
  MDBRow,
  MDBCol,
  MDBIcon,
  MDBBtn,
  MDBView,
  MDBContainer,
  MDBCard,
  MDBCardBody,
  MDBInput,
  MDBAnimation,
  MDBSmoothScroll
} from 'mdbreact';
import './BankingLoginPage.css';

class ClassicFormPage extends React.Component {
  state = {
    username: 'Mustermann',
    password: '1234'
  };



  submitHandler = event => {
    event.preventDefault();
    event.target.className += ' was-validated';
  };

  changeHandler = event => {
    const { name, value } = event.target;
    this.setState({ ...this.state, [name]: value });
  };

  state = {
    collapseID: ''
  };

  toggleCollapse = collapseID => () =>
    this.setState(prevState => ({
      collapseID: prevState.collapseID !== collapseID ? collapseID : ''
    }));



  render() {

    const { username, password } = this.state;

    function Example() {

      const [text, setText] = useState('Sie haben ein falsches Passwort eingegeben. Bitte versuchen Sie es erneut oder wenden sich bei weiteren Problemen an unseren Kundenservice.');
  /* const [pitch, setPitch] = useState(1); */
  /* const [rate, setRate] = useState(1); */
  const [voiceIndex, setVoiceIndex] = useState(null);
  const onEnd = () => {
    // You could do something here after speaking has finished
  };
  const {
    speak,
    cancel,
    speaking,
    supported,
    voices
  } = useSpeechSynthesis({ onEnd });

  const voice = voices[voiceIndex] || null;

  
    
      return (
        <MDBContainer>
        <div>
          
        {/* <h2>Speech Synthesis</h2> */}
        { !supported && (
          <p>Oh no, it looks like your browser doesn&#39;t support Speech Synthesis.</p>
        )}
        {supported && (
          
          <React.Fragment>
            
            {/* <p>
              {`Type a message below then click 'Speak'
                and SpeechSynthesis will read it out.`}
            </p>
            <label htmlFor="voice">
              Voice
            </label> */}
            
            <select
              id="voice"
              name="voice"
              
              value={voiceIndex || ''}
              onChange={(event) => { setVoiceIndex(event.target.value); }}
            >
              <option value="">Default</option>
              {voices.map((option, index) => (
                <option key={option.voiceURI} value={index}>
                  {`${option.lang} - ${option.name}`}
                </option>
              ))}
            </select>
            
            
         
            
            {/* <label htmlFor="message">
              Message
            </label> */}
            <MDBInput type="textarea"  style={{color:"white", padding: "10px"}}
              id="message"
              name="message"
              rows={3}
              value={text}
              onChange={(event) => { setText(event.target.value); }}
            />
            <br></br>
            { speaking
              ? (
                <MDBBtn color="primary" onClick={cancel}>
                  Stop
                </MDBBtn>
              ) : (
                <MDBBtn className="indigo" type="button" onClick={() => speak({ text, voice })}>
                  Speak
                </MDBBtn>
              )
            }
          </React.Fragment>
          
        )}
     
        </div>
        </MDBContainer>
      );
    }

    return (

      <div id='bankloginpage'>
        <MDBSmoothScroll
            size='lg'
            fixed
            floating
            className='morpheus-den-gradient'
            to='page-top'
            smooth
          >
            <MDBIcon icon='angle-up' />
          </MDBSmoothScroll> 

        <MDBView id="page-top">
          <MDBMask className='d-flex justify-content-center align-items-center morpheus-den-gradient' style={{opacity: '.9'}}/>
          <MDBContainer
            style={{ height: '100%', width: '100%', paddingTop: '8rem' }}
            className='mt-5  d-flex justify-content-center align-items-center'
          >
            <MDBRow>
              <MDBAnimation
                type='fadeInLeft'
                delay='.3s'
                className='white-text text-center text-md-left col-md-6  mb-5 '
              >
                <MDBCard className='rounded z-depth-5 border' style={{background: 'transparent'}}>
                  <MDBCardBody>
                  <h3 className='text-left'>
                          <MDBIcon icon='user' /> Zu Ihrem Konto:
                      </h3>
                      <hr className='hr-light' />
                <h6 className='mb-4'>
                  Bitte loggen Sie sich in ein um Ihre Transaktionen, Umsätze oder neuen Nachrichten zu sehen
                </h6>
                <hr className='hr-light' />
                <h6 className='mt-2 mb-4'>Sollten Sie noch kein Konto bei uns haben können Sie sich auch hier für einen Benutzer-Account registrieren.</h6>
                <MDBBtn outline color='white' href='/banking-register'>
                  Register
                </MDBBtn>
                  </MDBCardBody>
                </MDBCard>              
              </MDBAnimation>

              <MDBCol md='6' xl='5' className='mb-4'>
                <MDBAnimation type='fadeInRight' delay='.3s' header='Tooltip'>
                  <form
                    className='needs-validation'
                    onSubmit={this.submitHandler}
                    noValidate
                  >
                    <MDBCard id='classic-card' className='morpheus-den-gradient rounded z-depth-5 border' >
                      <MDBCardBody className='white-text'>
                        <h3 className='text-center'>
                          <MDBIcon icon='user' /> Login:
                      </h3>
                        <hr className='hr-light' />
                        <MDBInput
                          value={username}
                          onChange={this.changeHandler}
                          id='defaultFormRegisterUsernameEx40'
                          name='username'
                          type='text'
                          className='white-text mb-5'
                          iconClass='white-text'
                          label='Username'
                          icon='user'
                          required
                        >
                          <div style={{ top: '50px', right:'0' }} className='invalid-tooltip'>
                            Please provide a valid username.
                         </div>
                          <div style={{ top: '50px', right:'0' }} className='valid-tooltip'>
                            Well done!
                         </div>
                        </MDBInput>
                        <MDBInput
                          value={password}
                          onChange={this.changeHandler}
                          id='defaultFormRegisterPasswordEx40'
                          name='password'
                          className='white-text mb-5'
                          iconClass='white-text'
                          label='Your password'
                          icon='lock'
                          type='password'
                          required
                        >
                          <div style={{ top: '50px', right:'0' }} className='invalid-tooltip'>
                            Please enter a valid password.
                         </div>
                          <div style={{ top: '50px', right:'0' }} className='valid-tooltip'>
                            Looks good!
                         </div>
                        </MDBInput>
                        <div className='text-center mt-4 black-text'>
                          <MDBBtn outline color='white' type='submit' href="/banking-overview">Login</MDBBtn>
                          <Example />
                        </div>
                      </MDBCardBody>
                    </MDBCard>
                  </form>
                </MDBAnimation>
              </MDBCol>
            </MDBRow>
          </MDBContainer>
        </MDBView>

        <MDBContainer>
          <MDBRow className='py-5'>
            <MDBCol md='12' className='text-center'>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </div>
    );
  }
}

export default ClassicFormPage;
