import React from 'react';
import {
  MDBRow,
  MDBCol,
  MDBSticky,
  MDBStickyContent,
  MDBTreeview,
  MDBTreeviewList,
  MDBTreeviewItem,
  MDBContainer,
  MDBAnimation,
  Link
} from 'mdbreact';
import './AppPage.css';


class TreeNavigation extends React.Component {
  state = {
    collapsed: false
  };

  handleTogglerClick = () => {
    const { collapsed } = this.state;
    this.setState({
      collapsed: !collapsed
    });
  };

  componentDidMount() {
    document.querySelector('nav').style.height = '65px';
  }

  componentWillUnmount() {
    document.querySelector('nav').style.height = 'auto';
  }

  render() {
    const sidebar = { float: 'left', width: '330px' };
    return (
      <div id='apppage'>        
          <MDBContainer
            style={{ height: '100%', width: '100%' }}
            className='d-flex justify-content-center white-text align-items-center'
          >
            <MDBRow>
              <MDBCol md='6' className='text-center text-md-left  mb-5'>
                <MDBAnimation type='fadeInLeft' delay='.3s'>                 
                  <div style={sidebar}>
                      <MDBStickyContent style={{ height: '800px' }}>
                        <MDBSticky>
                          {({
                            isSticky,
                            wasSticky,
                            style,
                            distanceFromTop,
                            distanceFromBottom,
                            calculatedHeight
                          }) => {
                            return (
                              <div
                                style={{
                                  ...style
                                }}
                              >
                                <MDBTreeview
                                  theme='animated'
                                  header='Navigation'
                                  className='w-20 morpheus-den-gradient border-top border-bottom z-depth-5 white-text d-none d-md-block'
                                  getValue={value => console.log(value)}                                 
                                >
                                  <MDBTreeviewList title='Tagesgeld' far open>
                                  <Link to='/banking-overview' target='_parent' ><MDBTreeviewItem icon='eye' title='Auszahlung' far /></Link>
                                  <Link to='/banking-register' target='_parent' ><MDBTreeviewItem icon='eye' title='Geplante Auszahlungen' far /></Link>
                                    <MDBTreeviewItem icon='edit' title='Umsätze anzeigen' far />
                                    <MDBTreeviewList icon='edit' title='Tagesgeld bearbeiten' far open>
                                      <MDBTreeviewItem icon='edit' title='Tagesgeld ändern' far />
                                      <MDBTreeviewItem icon='edit' title='Tagesgeld löschen' opened />
                                      <MDBTreeviewItem icon='edit' title='Tagesgeld anlegen' />
                                    </MDBTreeviewList>
                                  </MDBTreeviewList>
                                  <MDBTreeviewList title='Festgeld' far>
                                    <MDBTreeviewItem title='Umsätze anzeigen' far />
                                    <MDBTreeviewItem title='Prolongation' far />
                                    <MDBTreeviewList icon='edit' title='Tagesgeld bearbeiten' far open>
                                      <MDBTreeviewItem title='Festgeld ändern' far />
                                      <MDBTreeviewItem title='Festgeld löschen' far />
                                      <MDBTreeviewItem title='Festgeld anlegen' far />
                                    </MDBTreeviewList>

                                  </MDBTreeviewList>
                                  <MDBTreeviewList title='Postbox' far>
                                    <MDBTreeviewItem title='Neue Nachrichten' far />
                                    <MDBTreeviewItem title='Gelesene Nachrichten' far />
                                    <MDBTreeviewItem title='Nachrichten schreiben' far />
                                  </MDBTreeviewList>
                                  <MDBTreeviewList title='Service' far>
                                    <MDBTreeviewItem icon='eye' title='Persönliche Daten' />
                                    <MDBTreeviewItem icon='edit' title='Persönliche Daten bearbeiten' far />
                                    <MDBTreeviewItem icon='eye' title='Benutzerkennung und Passwort' />
                                    <MDBTreeviewItem icon='eye' title='Freistellungsauftrag' />
                                    <MDBTreeviewItem icon='eye' title='Einwilligungen' />
                                    <MDBTreeviewItem icon='eye' title='Nichtveranlagungsbescheinigung' />
                                  </MDBTreeviewList>
                                </MDBTreeview>
                              </div>
                            );
                          }}
                        </MDBSticky>
                      </MDBStickyContent>
                    </div>
                </MDBAnimation>
              </MDBCol>             
            </MDBRow>
          </MDBContainer>        
      </div>
    );
  }
}

export default TreeNavigation;
