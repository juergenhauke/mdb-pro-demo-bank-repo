import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import {
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavItem,
  MDBNavLink,
  MDBTabPane,
  MDBTabContent,
  MDBNav,
  MDBLink,
  MDBNavbarToggler,
  MDBCollapse,
  MDBMask,
  MDBRow,
  MDBCol,
  MDBIcon,
  MDBBtn,
  MDBView,
  MDBContainer,
  MDBCardBody,
  MDBFormInline,
  MDBAnimation,
  MDBSmoothScroll,
  MDBTreeview,
  MDBTreeviewList,
  MDBTreeviewItem,
  MDBCardTitle,
  MDBCardText,
} from 'mdbreact';
import './BankingLoginPage.css';
import SectionContainer from '../../../components/sectionContainer';

class ClassicFormPage extends React.Component {
  state = {
    activeItem: '1',
    activeItem2: '1',
    activeItemJustified: '1',
    activeItemClassicTabs1: '1',
    activeItemClassicTabs2: '1',
    activeItemClassicTabs3: '1',
    activeItemOuterTabs: '1',
    activeItemInnerPills: '1'
  };

  toggle = tab => e => {
    const { activeItem } = this.state;
    if (activeItem !== tab) {
      this.setState({
        activeItem: tab
      });
    }
  };

  toggle2 = tab => e => {
    const { activeItem2 } = this.state;
    if (activeItem2 !== tab) {
      this.setState({
        activeItem2: tab
      });
    }
  };

  toggleJustified = tab => e => {
    const { activeItemJustified } = this.state;
    if (activeItemJustified !== tab) {
      this.setState({
        activeItemJustified: tab
      });
    }
  };

  toggleClassicTabs1 = tab => e => {
    const { activeItemClassicTabs1 } = this.state;
    if (activeItemClassicTabs1 !== tab) {
      this.setState({
        activeItemClassicTabs1: tab
      });
    }
  };

  toggleClassicTabs2 = tab => e => {
    const { activeItemClassicTabs2 } = this.state;
    if (activeItemClassicTabs2 !== tab) {
      this.setState({
        activeItemClassicTabs2: tab
      });
    }
  };

  toggleClassicTabs3 = tab => e => {
    const { activeItemClassicTabs3 } = this.state;
    if (activeItemClassicTabs3 !== tab) {
      this.setState({
        activeItemClassicTabs3: tab
      });
    }
  };

  toggleOuterTabs = tab => e => {
    const { activeItemOuterTabs2 } = this.state;
    if (activeItemOuterTabs2 !== tab) {
      this.setState({
        activeItemOuterTabs: tab
      });
    }
  };

  toggleInnerPills = tab => e => {
    const { activeItemInnerPills } = this.state;
    if (activeItemInnerPills !== tab) {
      this.setState({
        activeItemInnerPills: tab
      });
    }
  };
  state = {
    username: 'Mustermann',
    password: '1234'
  };





  submitHandler = event => {
    event.preventDefault();
    event.target.className += ' was-validated';
  };

  changeHandler = event => {
    const { name, value } = event.target;
    this.setState({ ...this.state, [name]: value });
  };

  state = {
    collapseID: ''
  };

  toggleCollapse = collapseID => () =>
    this.setState(prevState => ({
      collapseID: prevState.collapseID !== collapseID ? collapseID : ''
    }));

  componentDidMount() {
    document.querySelector('nav').style.height = '65px';
  }

  componentWillUnmount() {
    document.querySelector('nav').style.height = 'auto';
  }

  render() {
    const {
      activeItemOuterTabs,
      activeItemInnerPills
    } = this.state;
    const { collapseID } = this.state;
    const overlay = (
      <div
        id='sidenav-overlay'
        style={{ backgroundColor: 'transparent' }}
        onClick={this.toggleCollapse('navbarCollapse')}
      />
      
    );
    return (

      <div id='bankoverviewpage'>
        <Router>
          <div>
            <MDBSmoothScroll
              size='lg'
              fixed
              floating
              className='morpheus-den-gradient'
              to='page-top'
              smooth
            >
              <MDBIcon icon='angle-up' />
            </MDBSmoothScroll>
            <MDBNavbar
              color='banking-primary-color'
              dark
              expand='md'
              fixed='top'
              scrolling
              transparent
              style={{ marginTop: '65px' }}
              className='morpheus-den-gradient border-bottom border-top z-depth-5'
            >
              <MDBContainer>
                <MDBNavbarBrand>
                  <strong className='white-text'>
                    <MDBLink to='./../banking-sections' target="_parent" className="text-white">Demo Bank</MDBLink>
                  </strong>
                </MDBNavbarBrand>
                <MDBNavbarToggler
                  onClick={this.toggleCollapse('navbarCollapse')}
                />
                <MDBCollapse id='navbarCollapse' isOpen={collapseID} navbar>
                  <MDBNavbarNav left>
                    <MDBNavItem active>
                      <MDBNavLink to='./banking-landing-page' target='_parent'>Home</MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBNavLink to='./banking-help' target='_parent'>Help</MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                      <MDBNavLink to='./banking-contactform' target='_parent'>Contact</MDBNavLink>
                    </MDBNavItem>
                  </MDBNavbarNav>
                  <MDBNavbarNav right>
                    <MDBNavItem>
                      <MDBFormInline waves>
                        <div className='md-form my-0'>
                          <input
                            className='form-control mr-sm-2'
                            type='text'
                            placeholder='Search'
                            aria-label='Search'
                          />
                        </div>
                      </MDBFormInline>
                    </MDBNavItem>
                  </MDBNavbarNav>
                </MDBCollapse>
              </MDBContainer>
            </MDBNavbar>
            {collapseID && overlay}
          </div>
        </Router>

        <MDBView id="page-top">
          <MDBMask className='d-flex justify-content-center align-items-center morpheus-den-gradient' />
          
          <MDBContainer
            style={{ height: '100%', width: '100%', paddingTop: '10rem' }}
            className='mt-5  d-flex justify-content-center align-items-center'
          >
            
            <MDBRow>
              <MDBCol md='6' xl='5' className='mb-4'>
                <MDBAnimation type='fadeInLeft' delay='.3s'>
                <MDBContainer>
                    <MDBCol md='4'>
                      <MDBTreeview
                        theme='animated'
                        header='Navigation'
                        className='w-20 morpheus-den-gradient border-top border-bottom z-depth-5 white-text'
                        getValue={value => console.log(value)}
                      >
                        <MDBTreeviewList title='Tagesgeld' far open>
                          <MDBTreeviewItem icon='eye' title='Auszahlung' far />
                          <MDBTreeviewItem icon='eye' title='Geplante Auszahlungen' far />
                          <MDBTreeviewItem icon='edit' title='Umsätze anzeigen' far />
                          <MDBTreeviewList icon='edit' title='Tagesgeld bearbeiten' far open>
                            <MDBTreeviewItem icon='edit' title='Tagesgeld ändern' far />
                            <MDBTreeviewItem icon='edit' title='Tagesgeld löschen' opened />
                            <MDBTreeviewItem icon='edit' title='Tagesgeld anlegen' />
                          </MDBTreeviewList>
                        </MDBTreeviewList>
                        <MDBTreeviewList title='Festgeld' far>
                          <MDBTreeviewItem title='Umsätze anzeigen' far />
                          <MDBTreeviewItem title='Prolongation' far />
                          <MDBTreeviewList icon='edit' title='Tagesgeld bearbeiten' far open>
                             <MDBTreeviewItem title='Festgeld ändern' far />
                             <MDBTreeviewItem title='Festgeld löschen' far />
                             <MDBTreeviewItem title='Festgeld anlegen' far />
                          </MDBTreeviewList>

                        </MDBTreeviewList>
                        <MDBTreeviewList title='Postbox' far>
                          <MDBTreeviewItem title='Neue Nachrichten' far />
                          <MDBTreeviewItem title='Gelesene Nachrichten' far />
                          <MDBTreeviewItem title='Nachrichten schreiben' far />
                        </MDBTreeviewList>
                        <MDBTreeviewList title='Service' far>
                          <MDBTreeviewItem icon='eye' title='Persönliche Daten' />
                          <MDBTreeviewItem icon='edit' title='Persönliche Daten bearbeiten' far />
                          <MDBTreeviewItem icon='eye' title='Benutzerkennung und Passwort' />
                          <MDBTreeviewItem icon='eye' title='Freistellungsauftrag' />
                          <MDBTreeviewItem icon='eye' title='Einwilligungen' />
                          <MDBTreeviewItem icon='eye' title='Nichtveranlagungsbescheinigung' />
                        </MDBTreeviewList>
                      </MDBTreeview>
                    </MDBCol>
                  </MDBContainer>
                  
                </MDBAnimation>
              </MDBCol>
              <MDBCol md='6' xl='5' className='mb-4'>
                <MDBAnimation type='fadeInRight' delay='.3s'>
                <SectionContainer className='morpheus-den-gradient border-top border-bottom z-depth-5 white-text'>
                <MDBNav tabs className='nav-justified morpheus-den-gradient' color='primary'>
                  <MDBNavItem>
                    <MDBNavLink
                      link
                      to='#'
                      active={activeItemOuterTabs === '1'}
                      onClick={this.toggleOuterTabs('1')}
                      role='tab'
                    >
                      <MDBIcon icon='user' /> Daily Deposit Account Overview
                    </MDBNavLink>
                  </MDBNavItem>
                  <MDBNavItem>
                    <MDBNavLink
                      link
                      to='#'
                      active={activeItemOuterTabs === '2'}
                      onClick={this.toggleOuterTabs('2')}
                      role='tab'
                    >
                      <MDBIcon icon='heart' /> Fixed Deposit Account Overview
                    </MDBNavLink>
                  </MDBNavItem>
                </MDBNav>
                <MDBTabContent
                  className='card mb-5'
                  activeItem={activeItemOuterTabs}
                >
                  <MDBTabPane tabId='1' role='tabpanel'>
                    <MDBRow>
                      <MDBCol md='3'>
                        <MDBNav pills color='primary' className='flex-column'>
                          <MDBNavItem>
                            <MDBNavLink
                              link
                              to='#'
                              active={activeItemInnerPills === '1'}
                              onClick={this.toggleInnerPills('1')}
                            >
                              Kontostand{' '}
                              <MDBIcon icon='download' className='ml-2' />
                            </MDBNavLink>
                          </MDBNavItem>
                          <MDBNavItem>
                            <MDBNavLink
                              link
                              to='#'
                              active={activeItemInnerPills === '2'}
                              onClick={this.toggleInnerPills('2')}
                            >
                              Letzte Einzahlung
                              <MDBIcon icon='file-alt' className='ml-2' />
                            </MDBNavLink>
                          </MDBNavItem>
                          <MDBNavItem>
                            <MDBNavLink
                              link
                              to='#'
                              active={activeItemInnerPills === '3'}
                              onClick={this.toggleInnerPills('3')}
                            >
                              Letzte Auszahlung
                              <MDBIcon icon='address-card' className='ml-2' />
                            </MDBNavLink>
                          </MDBNavItem>
                        </MDBNav>
                      </MDBCol>
                      <MDBCol md='9'>
                        <MDBTabContent activeItem={activeItemInnerPills}>
                          <MDBTabPane tabId='1'>
                            <h5 className='bank-text ml-5'>23.500,56 €</h5>
                          </MDBTabPane>
                          <MDBTabPane tabId='2'>
                            <h5 className='bank-text ml-5'>2456,45 €</h5>
                            <h6 className='bank-text ml-5'>Gehalt</h6>
                          </MDBTabPane>
                          <MDBTabPane tabId='3'>
                            <h5 className='bank-text ml-5'>Geplante Auszahlungen</h5>
                          </MDBTabPane>
                        </MDBTabContent>
                      </MDBCol>
                    </MDBRow>
                  </MDBTabPane>
                  <MDBTabPane tabId='2' role='tabpanel'>
                    <MDBRow>
                      <MDBCol md='6'>
                        <MDBCardBody>
                          <MDBCardTitle>Special Title Treatment</MDBCardTitle>
                          <MDBCardText>
                            With supporting text below as a natural lead-in to
                            additional content.
                          </MDBCardText>
                          <MDBBtn>Go somewhere</MDBBtn>
                        </MDBCardBody>
                      </MDBCol>
                      <MDBCol md='6'>
                        <MDBCardBody>
                          <MDBCardTitle>Special Title Treatment</MDBCardTitle>
                          <MDBCardText>
                            With supporting text below as a natural lead-in to
                            additional content.
                          </MDBCardText>
                          <MDBBtn>Go somewhere</MDBBtn>
                        </MDBCardBody>
                      </MDBCol>
                    </MDBRow>
                  </MDBTabPane>
                </MDBTabContent>
              </SectionContainer>
                </MDBAnimation>
              </MDBCol>
            </MDBRow>
          </MDBContainer>
        </MDBView>

        <MDBContainer>
          <MDBRow className='py-5'>
            <MDBCol md='12' className='text-center'>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </div>
    );
  }
}

export default ClassicFormPage;
