import React from 'react';
import {

  MDBNavItem,

  MDBLink,

  MDBMask,
  MDBRow,
  MDBCol,
  MDBIcon,
  MDBView,
  MDBContainer,
  MDBAnimation,
  MDBSticky,
  MDBStickyContent,
  MDBNav,
  MDBTabContent,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBTabPane,
  MDBBtn,
  MDBBadge,
  MDBCard,
  MDBCardImage,
  MDBTable,
  MDBTableHead,
  MDBTableBody,
  MDBSmoothScroll
} from 'mdbreact';
import './BankingOverviewPage.css';
import SectionContainer from '../../../components/sectionContainer';
import TreeNavigation from './TreeNavigation';

class ClassicFormPage extends React.Component {

  togglePills = (type, tab) => e => {
    const { items } = this.state;
    e.preventDefault();
    if (items[type] !== tab) {
      items[type] = tab;
      this.setState({
        items
      });
    }
  };

  toggle = nr => () => {
    const modalNumber = `modal${nr}`;
    this.setState({
      [modalNumber]: !this.state[modalNumber]
    });
  };

  submitHandler = event => {
    event.preventDefault();
    event.target.className += ' was-validated';
  };

  changeHandler = event => {
    const { name, value } = event.target;
    this.setState({ ...this.state, [name]: value });
  };

  state = {
    collapseID: '',
    items: {
      default: '1',
      justified: '1',
      dropdown: '1',
      vertical: '1',
      gradient: '1',
      rounded: '1',
      rounded2: '1',
      roundedGradient: '1',
      roundedGradient2: '1',
      roundedOutline: '1',
      roundedOutline2: '1',
      icons: '1',
      iconsRight: '1',
      content: '1',
      contentCard: '1',
      animation: '1',
      animationRO: '1',
      animationR: '1',
      modal: '1',
      outer: '1',
      inner: '1'
    }
  };

  render() {

    const data = {
      columnsTg: [
        {
          label: '#',
          field: 'id',
          sort: 'asc'
        },
        {
          label: 'Datum',
          field: 'date',
          sort: 'asc'
        },
        {
          label: 'Betrag',
          field: 'betrag',
          sort: 'asc'
        }
      ],
      columnsFg: [
        {
          label: '#',
          field: 'id',
          sort: 'asc'
        },
        {
          label: 'Datum',
          field: 'date',
          sort: 'asc'
        },
        {
          label: 'Betrag',
          field: 'betrag',
          sort: 'asc'
        }
      ],
      rowsTg: [
        {
          'id': 1,
          'first': '24.03.2020',
          'last': '+3456,34 €',
        },
        {
          'id': 2,
          'first': '25.03.2020',
          'last': '-401,00 €',
        },
        {
          'id': 3,
          'first': '26.03.2020',
          'last': '-18,75 €',
        },
        {
          'id': 4,
          'first': '27.03.2020',
          'last': '-46.31 €',
        },
        {
          'id': 5,
          'first': '28.03.2020',
          'last': '-13,56',
        },
        {
          'id': 6,
          'first': '28.03.2020',
          'last': '-23,56',
        }
      ],
      rowsFg: [
        {
          'id': 1,
          'first': '08.04.2020',
          'last': '+436,34 €',
        },
        {
          'id': 2,
          'first': '01.04.2020',
          'last': '+401,00 €',
        },
        {
          'id': 3,
          'first': '12.03.2020',
          'last': '+36,75 €',
        },
        {
          'id': 4,
          'first': '02.03.2020',
          'last': '+346.31 €',
        },
        {
          'id': 5,
          'first': '12.01.2020',
          'last': '+3,56',
        },
        {
          'id': 6,
          'first': '03.12.2019',
          'last': '+23,56',
        },
        {
          'id': 7,
          'first': '03.12.2019',
          'last': '+23,56',
        },
        {
          'id': 8,
          'first': '12.10.2019',
          'last': '+423,56',
        },
        {
          'id': 9,
          'first': '07.09.2019',
          'last': '+1223,56',
        }
      ]}
   
    const wrapper = { height: '800px', backgroundColor: 'rgba(0,0,0,.15)' };

    
    const headerStyle = {
      width: '100%',
      color: '#fff',
      padding: '10px 0',
      marginTop: '70px'

    };
    const mainWrapper = { width: '100%', margin: '0 auto' };
    const sidebar = { float: 'left', width: '330px', padding: '10px 0' };
    const article = {
      width: 'calc(100% - 330px)',
      padding: '0 0 0 40px',
      borderLeft: '1px solid #ccc',
      borderRight: '1px solid #ccc',
      float: 'left',
      color: '#fff'
    };

    window.addEventListener('scroll', function (e) {
      const nav = document.querySelector('nav');
      if (document.querySelector('.stickyContentDemo')) {

        nav.style.transition = 'background 0.5s ease-in-out, padding 0.5s ease-in-out,top 1s';
        if (
          document.body.scrollTop > 150 ||
          document.documentElement.scrollTop > 150
        ) {
          nav.style.top = '-65px';
        } else {
          nav.style.top = '0';
        }
      } else {
        nav.style.top = '0';
      }
    });

    const { items } = this.state;
    return (

      <div id='bankoverviewpage' className='stickyContentDemo'>
        <MDBSmoothScroll
            size='lg'
            fixed
            floating
            className='morpheus-den-gradient'
            to='page-top'
            smooth
          >
            <MDBIcon icon='angle-up' />
          </MDBSmoothScroll>
        

        <MDBView id="page-top">
          <MDBMask className='d-flex justify-content-center align-items-center morpheus-den-gradient' />

          <MDBContainer>
            <SectionContainer header='Demo' className=''>
              <div style={wrapper} className='mt-4'>
                
                  <div style={mainWrapper}>
                    <MDBStickyContent style={{ background: '#fff', height: '165px' }}>
                      <MDBSticky>
                        {({ style }) => {
                          return (
                            <div
                              style={{
                                ...style
                              }}
                            >
                              <div style={headerStyle}>
                                <h1>Account Dasboard</h1>
                                <h4>Alles auf einem Blick für Sie.</h4>
                              </div>
                            </div>
                          );
                        }}
                      </MDBSticky>
                    </MDBStickyContent>
                    <MDBAnimation type='fadeInLeft' delay='.3s' header='Tooltip'>
                    <div style={sidebar}>

                      <MDBStickyContent style={{ height: '800px' }}>
                        <MDBSticky>
                          {({
                            isSticky,
                            wasSticky,
                            style,
                            distanceFromTop,
                            distanceFromBottom,
                            calculatedHeight
                          }) => {
                            return (
                              <div
                                style={{
                                  ...style
                                }}
                              >
                                <TreeNavigation />                               
                              </div>
                            );
                          }}
                        </MDBSticky>
                      </MDBStickyContent>
                    </div>
                    </MDBAnimation>

                    <div style={article}>
                      <MDBStickyContent style={{ height: '820px', width: '100% !important' }}>
                        <MDBSticky>
                          {({ style }) => {
                            return (
                              <div
                                style={{
                                  ...style
                                }}
                              >
                                <MDBAnimation type='fadeInRight' delay='.3s' header='Tooltip'>
                                <MDBContainer className='morpheus-den-gradient z-depth-5 white-text mt-2 pb-2 pt-2 border' style={{}}>
                                  <MDBNav tabs className='nav-justified morpheus-den-gradient'>
                                    <MDBNavItem>
                                      <MDBLink to='#' role='tab' active={items['outer'] === '1'} link onClick={this.togglePills('outer', '1')}>
                                        <MDBIcon icon='envelope' /> Nachrichten <MDBBadge color="success" className="ml-2">3</MDBBadge>
                                        <MDBBadge color="warning" className="ml-2">37</MDBBadge>
                                        <MDBContainer>
                                        </MDBContainer>
                                      </MDBLink>
                                    </MDBNavItem>
                                    <MDBNavItem>
                                      <MDBLink to='#' role='tab' active={items['outer'] === '2'} link onClick={this.togglePills('outer', '2')}>
                                        <MDBIcon icon='heart' /> Accounts
                                      </MDBLink>
                                    </MDBNavItem>
                                  </MDBNav>
                                  <MDBTabContent className='card mb-5' activeItem={items.outer}>
                                    <MDBTabPane tabId='1' role='tabpanel'>
                                      <MDBRow>
                                        <MDBCol md='6'>
                                          <MDBNav pills color='indigo' className='flex-column '>
                                            <MDBNavItem className='mt-4'>
                                              <MDBLink to='#' active={items['inner'] === '1'} link onClick={this.togglePills('inner', '1')} className='text-left'>
                                                Neue Nachrichten <MDBIcon icon='envelope' className='ml-2' /><MDBBadge color="success" className="ml-2">2</MDBBadge>
                                              </MDBLink>
                                            </MDBNavItem>
                                            <MDBNavItem>
                                              <MDBLink to='#' active={items['inner'] === '2'} link onClick={this.togglePills('inner', '2')} className='text-left'>
                                                Ungelesene Nachrichten
                                                <MDBIcon icon='envelope-open' className='ml-2' /><MDBBadge color="success" className="ml-2">1</MDBBadge>
                                              </MDBLink>
                                            </MDBNavItem>
                                            <MDBNavItem>
                                              <MDBLink to='#' active={items['inner'] === '3'} link onClick={this.togglePills('inner', '3')} className='text-left'>
                                                Nachrichten Archiv
                                                 <MDBIcon icon='envelope-open' className='ml-2' /><MDBBadge color="warning" className="ml-2">37</MDBBadge>
                                              </MDBLink>
                                            </MDBNavItem>
                                          </MDBNav>
                                        </MDBCol>
                                        <MDBCol md='6'>
                                          <MDBTabContent activeItem={items.inner}>
                                            <MDBTabPane tabId='1'>
                                              <MDBCard className='mb-5'>

                                                <MDBCardBody className='morpheus-den-gradient white-text rounded'>
                                                  <a href='#!' className='activator waves-effect waves-light mr-4'>
                                                    <MDBIcon icon='share-alt' className='white-text' />
                                                  </a>
                                                  <MDBCardTitle className='h5-responsive'>Neue Nachricht 1</MDBCardTitle>
                                                  <hr className='hr-light' />
                                                  <MDBCardText className='white-text'>
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                                    eiusmod tempor incididunt ut labore et dolore magna aliqua...
                                                  </MDBCardText>
                                                  <a href='#!' className='black-text d-flex justify-content-end'>
                                                    <h6 className='white-text'>
                                                      mehr
                                                  <MDBIcon icon='angle-double-right' className='ml-2' />
                                                    </h6>
                                                  </a>
                                                </MDBCardBody>
                                              </MDBCard>
                                              <MDBCard>

                                                <MDBCardBody className='morpheus-den-gradient white-text rounded'>
                                                  <a href='#!' className='activator waves-effect waves-light mr-4'>
                                                    <MDBIcon icon='share-alt' className='white-text' />
                                                  </a>
                                                  <MDBCardTitle className='h5-responsive'>Neue Nachricht 2</MDBCardTitle>
                                                  <hr className='hr-light' />
                                                  <MDBCardText className='white-text'>
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                                    eiusmod tempor incididunt ut labore et dolore magna aliqua...
                                                  </MDBCardText>
                                                  <a href='#!' className='black-text d-flex justify-content-end'>
                                                    <h6 className='white-text'>
                                                      mehr
                                                  <MDBIcon icon='angle-double-right' className='ml-2' />
                                                    </h6>
                                                  </a>
                                                </MDBCardBody>
                                              </MDBCard>

                                            </MDBTabPane>
                                            <MDBTabPane tabId='2'>
                                              <MDBCard className='mb-5'>

                                                <MDBCardBody className='morpheus-den-gradient white-text rounded'>
                                                  <a href='#!' className='activator waves-effect waves-light mr-4'>
                                                    <MDBIcon icon='share-alt' className='white-text' />
                                                  </a>
                                                  <MDBCardTitle className='h5-responsive'>Neue Nachricht 1</MDBCardTitle>
                                                  <hr className='hr-light' />
                                                  <MDBCardText className='white-text'>
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                                    eiusmod tempor incididunt ut labore et dolore magna aliqua...
                                                  </MDBCardText>
                                                  <a href='#!' className='black-text d-flex justify-content-end'>
                                                    <h6 className='white-text'>
                                                      mehr
                                                  <MDBIcon icon='angle-double-right' className='ml-2' />
                                                    </h6>
                                                  </a>
                                                </MDBCardBody>
                                              </MDBCard>

                                            </MDBTabPane>
                                            <MDBTabPane tabId='3'>
                                              <MDBCardBody>
                                                <MDBCardTitle className='bank-text'>Zur Ihrer Postbox</MDBCardTitle>
                                                <MDBCardText>
                                                  <MDBBtn color='indigo' size='sm'>
                                                    click
                                                 </MDBBtn>
                                                </MDBCardText>
                                              </MDBCardBody>
                                            </MDBTabPane>
                                          </MDBTabContent>
                                        </MDBCol>
                                      </MDBRow>
                                    </MDBTabPane>
                                    <MDBTabPane tabId='2' role='tabpanel'>
                                      <MDBRow>
                                        <MDBCol md='6'>
                                          <MDBCard wide>
                                            <MDBCardImage
                                              className='view view-cascade gradient-card-header morpheus-den-gradient'
                                              cascade
                                              tag='div'
                                            >
                                              <h2 className='h5-responsive mb-2'>Tagesgeld</h2>
                                              <p>
                                                <MDBIcon icon='euro-sign' /> 25.657,98
                                             </p>
                                            </MDBCardImage>
                                            <MDBCardBody cascade className='text-center scrollbar scrollbar-morpheus-den'>
                                              
                                              <MDBTable>
                                               <MDBTableHead columns={data.columnsTg} />
                                                  <MDBTableBody rows={data.rowsTg} />
                                              </MDBTable>                                                                                        
                                            </MDBCardBody>
                                            <MDBBtn  className='mt-3 morpheus-den-gradient'>
                                                  <MDBIcon icon="chevron-right" className="mr-1" /> alle Umsätze
                                              </MDBBtn>
                                          </MDBCard>
                                        </MDBCol>
                                        <MDBCol md='6'>
                                          <MDBCard wide>
                                            <MDBCardImage
                                              className='view view-cascade gradient-card-header morpheus-den-gradient'
                                              cascade
                                              tag='div'
                                            >
                                              <h2 className='h5-responsive mb-2'>Festgeld</h2>
                                              <p>
                                                <MDBIcon icon='euro-sign' /> 5.407,00
                                             </p>
                                            </MDBCardImage>
                                            <MDBCardBody cascade className='text-center scrollbar scrollbar-morpheus-den'>
                                            <MDBTable>
                                               <MDBTableHead columns={data.columnsFg} />
                                                  <MDBTableBody rows={data.rowsFg} />
                                              </MDBTable>
                                              
                                            </MDBCardBody>
                                            <MDBBtn  className='mt-3 morpheus-den-gradient'>
                                                  <MDBIcon icon="chevron-right" className="mr-1" /> alle Umsätze
                                              </MDBBtn>
                                          </MDBCard>
                                        </MDBCol>
                                      </MDBRow>
                                    </MDBTabPane>
                                  </MDBTabContent>
                                </MDBContainer>

                                </MDBAnimation>
                                
                              </div>
                            );
                          }}
                        </MDBSticky>
                      </MDBStickyContent>
                    </div>
                  </div>
                
              </div>
            </SectionContainer>
          </MDBContainer>
        </MDBView>

        <MDBContainer>
          <MDBRow className='py-5'>
            <MDBCol md='12' className='text-center'>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </div>
    );
  }
}

export default ClassicFormPage;
