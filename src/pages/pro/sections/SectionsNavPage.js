import React from 'react';
import { 
  MDBEdgeHeader, 
  MDBContainer, 
  MDBRow, 
  MDBCol, 
  MDBJumbotron, 
  MDBIcon, 
  MDBAnimation, 
  MDBSmoothScroll,
  MDBBtn,
  MDBSideNavCat,
  MDBSideNavNav,
  MDBSideNav,
  MDBSideNavLink
 } from 'mdbreact';
import MenuLink from '../../../components/menuLink';

class SectionsNavPage extends React.Component {
  state = {
    sideNavLeft: false,
    sideNavRight: false
  };

 

  sidenavToggle = sidenavId => () => {
    const sidenavNr = `sideNav${sidenavId}`;
    this.setState({
      [sidenavNr]: !this.state[sidenavNr]
    });
  };

  scrollToTop = () => window.scrollTo(0, 0);
  render() {
    const { sideNavRight, sideNavLeft } = this.state;
    
  return (
    <>
    <MDBSmoothScroll
          size='lg'
          fixed
          floating
          className='morpheus-den-gradient'
          to='page-top'
          smooth
        >
          <MDBIcon icon='angle-up' />
        </MDBSmoothScroll>
      <MDBEdgeHeader color='indigo darken-3' className='sectionPage' id='page-top' />
      <div className="d-flex justify-content-between">
          <div className="p-2 text-left" style={{marginTop: '-50px'}}>
            <MDBBtn onClick={this.sidenavToggle('Left') } className='morpheus-den-gradient'>
              <MDBIcon size='lg' icon='bars' />
            </MDBBtn>
            {/* the left SideNav: */}
            <MDBSideNav
              
              triggerOpening={sideNavLeft}
              breakWidth={1300}
              className='morpheus-den-gradient'
            >
              <li>
                <div className='logo-wrapper sn-ad-avatar-wrapper'>
                  <a href='#!'>
                    <img
                      alt=''
                      src='https://web-complett.de/mdb-react-pro/personal/ich-kopf-200x200.png'
                      className='rounded-circle'
                    />
                    <span>Documentations</span>
                  </a>
                </div>
              </li>

              <MDBSideNavNav>
                <MDBSideNavLink to='/' topLevel>
                  <MDBIcon icon='home' className='mr-2' />
                  Home
                </MDBSideNavLink>
                
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/60031069/Form+Builder+-+React-Hooks' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Form Generator</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64389123/Speech+-+Api' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Speech Api</a>
                <MDBSideNavCat
                  name='Setup'
                  id='setup'
                  icon='chevron-right'
                >
                  <MDBSideNavLink to='/project-setup' target="_parent">Project Setup</MDBSideNavLink>
                  <MDBSideNavLink to='/scripts' target="_parent">Start / Build / Test</MDBSideNavLink>
                  <MDBSideNavLink to='/dependencies' target="_parent">Dependencies</MDBSideNavLink>
                  <MDBSideNavLink to='/webpack' target="_parent">Webpack</MDBSideNavLink>
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='React'
                  id='react'
                  icon='hand-pointer'
                  href='#'
                >
                  <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64356392/Create+React+App' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Create React App</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64356399/React+Router' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> React Router</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421963/ServiceWorker' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> ServiceWorker</a>
                </MDBSideNavCat>
                <MDBSideNavCat name='Design' id='about' icon='eye'>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421898/MDBootstrap' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> MDBootstrap</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64356365/Colormanagement' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Color-Management</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421915/Fontmanagement' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Font-Management</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421922/Templates' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Templates</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421929/Responsive' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Responsive</a>
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='Contact me'
                  id='contact-me'
                  icon='envelope'
                >
                  <MDBSideNavLink to='/sections/rws-contact' target="_parent">Contact</MDBSideNavLink>
                </MDBSideNavCat>
              </MDBSideNavNav>
            </MDBSideNav>
          </div>
          <div className="p-2 text-left">Flex item 2</div>
          <div className="p-2 text-left" style={{marginTop: '-50px'}}>
            <MDBBtn onClick={this.sidenavToggle('Right')} className='morpheus-den-gradient'>
              <MDBIcon size='lg' icon='bars' />
            </MDBBtn>
            {/* the right SideNav: */}
            <MDBSideNav
             
              triggerOpening={sideNavRight}
              className='morpheus-den-gradient'
              right
              breakWidth={1300}
            >
              <li>
                <div className='logo-wrapper sn-ad-avatar-wrapper'>
                  <a href='#!'>
                    <img
                      alt=''
                      src='https://web-complett.de/mdb-react-pro/personal/ich-kopf-200x200.png'
                      className='rounded-circle'
                    />
                    <span>Examples</span>
                  </a>
                </div>
              </li>
              <li>
                <ul className='social'>
                  <li>
                    <MDBIcon brand icon='facebook' />
                  </li>
                  <li>
                    <MDBIcon brand icon='pinterest' />
                  </li>
                  <li>
                    <MDBIcon brand icon='google-plus' />
                  </li>
                  <li>
                    <MDBIcon brand icon='twitter' />
                  </li>
                </ul>
              </li>
              <MDBSideNavNav>
              <MDBSideNavCat
                  name='Demo Sections'
                  id='registration-login--docs'
                  icon='hand-pointer'
                >
                  <MDBSideNavLink to='/sections/app' target='_parent'>App</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/blog' target='_parent'>Blog</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/call-to-action-intro' target='_parent'>Call to action</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/classicform' target='_parent'>Classic form</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/contact' target='_parent'>Contact</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/contactform' target='_parent'>Contact form</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/ecommerce' target='_parent'>Ecommerce</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/features' target='_parent'>Features</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/magazine' target='_parent'>Magazin</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/minimalistic-intro' target='_parent'>Minimalistic intro</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/parallax-intro' target='_parent'>Parallax intro</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/projects' target='_parent'>Projects</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/social' target='_parent'>Social</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/team' target='_parent'>Teams</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/testimonials' target='_parent'>Testimonials</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/videobackground' target='_parent'>Videobackground</MDBSideNavLink>
                </MDBSideNavCat>
              
                <MDBSideNavCat
                  name='Demo Components'
                  id='components-docs'
                  icon='chevron-right'
                >
                  <MDBSideNavLink to='/css' target='_parent'>CSS</MDBSideNavLink>
                  <MDBSideNavLink to='/components' target='_parent'>Components</MDBSideNavLink>
                  <MDBSideNavLink to='/advanced' target='_parent'>Advanced</MDBSideNavLink>
                  <MDBSideNavLink to='/navigation' target='_parent'>Navigation</MDBSideNavLink>
                  <MDBSideNavLink to='/forms' target='_parent'>Forms</MDBSideNavLink>
                  <MDBSideNavLink to='/tables' target='_parent'>Tables</MDBSideNavLink>
                  <MDBSideNavLink to='/modals' target='_parent'>Modal</MDBSideNavLink>
                  <MDBSideNavLink to='/addons' target='_parent'>Addons</MDBSideNavLink>
                  <MDBSideNavLink to='/sections' target='_parent'>Sections</MDBSideNavLink>
                  
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='Demo Plugins'
                  id='components-docs'
                  icon='chevron-right'
                >
                  <MDBSideNavLink to='/plugins/pro/plugins-wysiwyg' target='_parent'>WYSIWYG</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-tableeditor' target='_parent'>Table Editor</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-sortable' target='_parent'>Sortable</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-fullcalendar' target='_parent'>Fullcalendar</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-colorpicker' target='_parent'>Colorpicker</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-fileupload' target='_parent'>Fileupload</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-filter' target='_parent'>Filter</MDBSideNavLink>
                  
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='Demo Forms'
                  id='registration-login--docs'
                  icon='hand-pointer'
                >
                  <MDBSideNavLink>Registration Forms</MDBSideNavLink>
                  <MDBSideNavLink>Login Forms</MDBSideNavLink>
                  <MDBSideNavLink>Registration/Login Forms</MDBSideNavLink>
                  <MDBSideNavLink>Inline Forms</MDBSideNavLink>
                  <MDBSideNavLink>Modal Forms</MDBSideNavLink>
                  <MDBSideNavLink>Vaklidation Forms</MDBSideNavLink>
                </MDBSideNavCat>
                              
              </MDBSideNavNav>
            </MDBSideNav>
          </div>
        </div>   
      <MDBAnimation type='zoomIn' duration='500ms'>
        <MDBContainer>
          <MDBRow>
            <MDBCol md='8' className='mt-3 mx-auto'>
              <MDBJumbotron>
                <h1 className='text-center'>
                  <MDBIcon icon='th' className='indigo-text mr-2' />
                  Sections
                </h1>
                <ul className='list-unstyled example-components-list'>
                  <h5 className='grey-text'><strong className='indigo-text'>Intros</strong></h5>
                  <MenuLink to='/sections/app' title='App intro' />
                  <MenuLink to='/sections/contactform' title='Contact Form intro' />
                  <MenuLink to='/sections/classicform' title='Classic Register Form intro' />
                  <MenuLink to='/sections/videobackground' title='Video Background intro' />
                  <MenuLink to='/sections/minimalistic-intro' title='Minimalistic Intro' />
                  <MenuLink to='/sections/parallax-intro' title='Parallax Effect Intro' />
                  <MenuLink to='/sections/call-to-action-intro' title='Call to Action Intro' />
                  <MenuLink to='/sections/contact' title='Contact' />
                  <MenuLink to='/sections/blog' title='Blog' />
                  <MenuLink to='/sections/ecommerce' title='E-commerce' />
                  <MenuLink to='/sections/features' title='Features' />
                  <MenuLink to='/sections/magazine' title='Magazine' />
                  <MenuLink to='/sections/projects' title='Projects' />
                  <MenuLink to='/sections/social' title='Social' />
                  <MenuLink to='/sections/team' title='Team' />
                  <MenuLink to='/sections/testimonials' title='Testimonials' />
                </ul>
              </MDBJumbotron>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </MDBAnimation>
    </>
  );
}
}

export default SectionsNavPage;
