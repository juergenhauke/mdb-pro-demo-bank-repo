import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import {
  MDBInput,
  MDBNavbar,
  MDBNavbarNav,
  MDBNavItem,
  MDBNavLink,
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem,
  MDBIcon,
  MDBSideNavLink,
  MDBSideNavCat,
  MDBSideNavNav,
  MDBSideNav,
  MDBContainer,
  MDBAnimation,
  MDBView,
  MDBMask,
  MDBAlert,
  MDBBreadcrumb,
  MDBBreadcrumbItem,
  MDBSmoothScroll,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBRotatingCard,
  MDBCardUp,
  MDBAvatar
} from 'mdbreact';
import DocsLink from '../../components/docsLink';
import SectionContainer from '../../components/sectionContainer';
import DashboardCharts from '../../pages/pro/DashboardCharts';
import './DashboardPage.css';




class DoubleNavigationPage extends Component {
  state = {
    flipped1: false,
    flipped2: false,
    flipped3: false,
    flipped4: false
  };

  handleFlipping = id => () => {
    const cardId = `flipped${id}`;
    this.setState({ [cardId]: !this.state[cardId] });
  };
  state = {
    toggleStateA: false,
    breakWidth: 1300,
    windowWidth: 0
  };

  componentDidMount = () => {
    this.handleResize();
    window.addEventListener('resize', this.handleResize);
  };

  componentWillUnmount = () => {
    window.removeEventListener('resize', this.handleResize);
  };

  handleResize = () =>
    this.setState({
      windowWidth: window.innerWidth
    });

  handleToggleClickA = () => {
    const { toggleStateA } = this.state;
    this.setState({
      toggleStateA: !toggleStateA
    });
  };

  render() {
    const { flipped1, flipped2, flipped3, flipped4 } = this.state;
    const { breakWidth, toggleStateA, windowWidth } = this.state;
    const navStyle = {
      paddingLeft: windowWidth > breakWidth ? '210px' : '16px'
    };
    const mainStyle = {
      margin: '0 6%',
      paddingTop: '5.5rem',
      paddingLeft: windowWidth > breakWidth ? '240px' : '0',
    };
    const specialCaseNavbarStyles = {
      WebkitBoxOrient: 'horizontal',
      flexDirection: 'row'
    };
    return (
      <Router>
        <div className='fixed-sn' id='dashboard-page'>
          <MDBSideNav
            logo='https://mdbootstrap.com/img/logo/mdb-transparent.png'
            triggerOpening={toggleStateA}
            breakWidth={breakWidth}
            bg='https://mdbootstrap.com/img/Photos/Others/sidenav1.jpg'
            mask='strong'
            fixed
            className='morpheus-den-gradient'
          >
            <li>
              <ul className='social'>
                <li>
                  <a href='#!'>
                    <MDBIcon brand icon='facebook' />
                  </a>
                </li>
                <li>
                  <a href='#!'>
                    <MDBIcon brand icon='pinterest' />
                  </a>
                </li>
                <li>
                  <a href='#!'>
                    <MDBIcon brand icon='google-plus' />
                  </a>
                </li>
                <li>
                  <a href='#!'>
                    <MDBIcon brand icon='twitter' />
                  </a>
                </li>
              </ul>
            </li>
            <MDBInput
              type='text'
              hint='Search'
              style={{
                color: '#fff',
                padding: '8px 10px 8px 30px',
                boxSizing: 'border-box'
              }}
            />
            <MDBSideNavNav>
              <MDBSideNavCat name='Tagesgeld' id='tagesgeld' icon='chevron-right'>
                <MDBSideNavLink to='/navigation/pro/tagesgeld-overview' target='_parent'>Übersicht</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/tagesgeld-auszahlung' target='_parent'>Auszahlung</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/tagesgeld-geplante-auszahlungen' target='_parent'>Geplante Auszahlung</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/tagesgeld-umsaetze-anzeigen' target='_parent'>Umsätze anzeigen</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/tagesgeld-bearbeiten' target='_parent'>Tagesgeld bearbeiten</MDBSideNavLink>
              </MDBSideNavCat>
              <MDBSideNavCat name='Festgeld' id='instruction-cat' icon='hand-pointer'>
                <MDBSideNavLink to='/navigation/pro/festgeld-overview' target='_parent'>Übersicht</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/festgeld-umsaetze-anzeigen' target='_parent'>Umsätze anzeigen</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/festgeld-prolongation' target='_parent'>Prolongation</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/festgeld-bearbeiten' target='_parent'>Festgeld bearbeiten</MDBSideNavLink>
              </MDBSideNavCat>
              <MDBSideNavCat name='Postbox' id='about-cat' icon='eye'>
                <MDBSideNavLink to='/navigation/pro/postbox-neue-nachrichten' target='_parent'>Neue Nachrichten</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/postbox-gelesene-nachrichten' target='_parent'>Gelesene Nachrichten</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/postbox-nachrichten-schreiben' target='_parent'>Nachrichten schreiben</MDBSideNavLink>
              </MDBSideNavCat>
              <MDBSideNavCat name='Service' id='contact-me-cat' icon='envelope'>
                <MDBSideNavLink to='/navigation/pro/service-persoenliche-daten' target='_parent'>Persönliche Daten</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/service-freistellungsauftrag' target='_parent'>Freistellungsauftrag</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/service-einwilligungen' target='_parent'>Einwilligungen</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/service-nichtveranlagungsbescheinigung' target='_parent'>Nichtveranlagungsbescheinigung</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/service-chat' target='_parent'>Hilfe Chat</MDBSideNavLink>
              </MDBSideNavCat>
            </MDBSideNavNav>
          </MDBSideNav>
          <MDBNavbar style={navStyle} double expand='md' fixed='top' scrolling className='morpheus-den-gradient'>
            <MDBNavbarNav left>
              <MDBNavItem>
                <div
                  onClick={this.handleToggleClickA}
                  key='sideNavToggleA'
                  style={{
                    lineHeight: '32px',
                    marginRight: '1em',
                    verticalAlign: 'middle'
                  }}
                >
                  <MDBIcon icon='bars' color='white' size='2x' />
                </div>
              </MDBNavItem>
              <MDBNavItem className='d-none d-md-inline' style={{ paddingTop: 5, color: '#ffffff' }}>
                MyDemoBank
              </MDBNavItem>
            </MDBNavbarNav>
            <MDBNavbarNav right style={specialCaseNavbarStyles}>
              <MDBNavItem active>
                <MDBNavLink to='#!' link>
                  <MDBIcon icon='envelope' className='d-inline-inline' />{' '}
                  <div className='d-none d-md-inline'>Contact</div>
                </MDBNavLink>
              </MDBNavItem>
              <MDBNavItem>
                <MDBNavLink to='#!' link>
                  <MDBIcon icon='comments' className='d-inline-inline' />{' '}
                  <div className='d-none d-md-inline'>Support</div>
                </MDBNavLink>
              </MDBNavItem>
              <MDBNavItem>
                <MDBNavLink to='#!' link>
                  <MDBIcon icon='user' className='d-inline-inline' /> <div className='d-none d-md-inline'>Account</div>
                </MDBNavLink>
              </MDBNavItem>
              <MDBNavItem>
                <MDBDropdown>
                  <MDBDropdownToggle nav caret>
                    <div className='d-none d-md-inline'>Dropdown</div>
                  </MDBDropdownToggle>
                  <MDBDropdownMenu right>
                    <MDBDropdownItem href='#!'>Action</MDBDropdownItem>
                    <MDBDropdownItem href='#!'>Another Action</MDBDropdownItem>
                    <MDBDropdownItem href='#!'>Something else here</MDBDropdownItem>
                    <MDBDropdownItem href='#!'>Something else here</MDBDropdownItem>
                  </MDBDropdownMenu>
                </MDBDropdown>
              </MDBNavItem>
            </MDBNavbarNav>
          </MDBNavbar>
          <MDBView id='page-top'>
            <MDBSmoothScroll
              size='lg'
              fixed
              floating
              className='morpheus-den-gradient'
              to='page-top'
              smooth
            >
              <MDBIcon icon='angle-up' />
            </MDBSmoothScroll>
            <MDBMask className='white-text gradient' />
            <main style={mainStyle}>

              <MDBContainer>
                <MDBAlert color="warning" dismiss>

                  <DocsLink
                    title='Documentation Confluence'
                    href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/69271553/Dashboard'
                  />
          Link zum <a href="https://webcomplett.atlassian.net/browse/MDB-2" target="_blank" rel="noopener noreferrer" className="alert-link">Jira Ticket</a>.

                  </MDBAlert>
              </MDBContainer>
              <MDBBreadcrumb className='morpheus-den-gradient mt-5'>
                    <MDBBreadcrumbItem>Tagesgeld</MDBBreadcrumbItem>
                    <MDBBreadcrumbItem active>Overview</MDBBreadcrumbItem>
                  </MDBBreadcrumb>
                  <h2 className='mt-5 morpheus-den-gradient z-depth-5 white-text p-5 border'>Ihre Ansprechpartner</h2>
              <SectionContainer flexCenter className='mt-5 morpheus-den-gradient z-depth-5 white-text p-5 border'>
                         
                <MDBCol md="3" sm="12" style={{ minHeight: '26rem', maxWidth: '22rem' }}>
                  
                  <MDBRotatingCard
                    flipped={flipped1}
                    className='text-center h-100 w-100'
                  >
                    <MDBCard className='face front'>
                      <MDBCardUp>
                        <img
                          className='card-img-top'
                          src='https://mdbootstrap.com/img/Photos/Others/images/82.jpg'
                          alt=''
                        />
                      </MDBCardUp>
                      <MDBAvatar className='mx-auto white' circle>
                        <img
                          src='https://web-complett.de/mdb-react-pro/placeholder/placeholder-women-676x676.png'
                          alt=''
                          className='rounded-circle'
                        />
                      </MDBAvatar>
                      <MDBCardBody>
                        <h4 className='font-weight-bold mb-3 blue-text'>Marie Johnson</h4>
                        <p className='font-weight-bold blue-text'>Ihre Kundenberaterin</p>
                        <a
                          href='#!'
                          className='rotate-btn text-dark'
                          data-card='card-1'
                          onClick={this.handleFlipping(1)}
                        >
                          <MDBIcon icon='redo' /> Click here to rotate
                  </a>
                      </MDBCardBody>
                    </MDBCard>
                    <MDBCard className='face back' style={{ height: '400px' }}>
                      <MDBCardBody>
                        <h4 className='font-weight-bold black-text'>About me</h4>
                        <hr />
                        <p className='black-text'>
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                          Maxime quae, dolores dicta. Blanditiis rem amet repellat,
                          dolores nihil quae in mollitia asperiores ut rerum
                          repellendus, voluptatum eum, officia laudantium quaerat?
                  </p>
                        <hr />
                        <ul className='list-inline py-2'>
                          <li className='list-inline-item'>
                            <a href='#!' className='p-2 fa-lg fb-ic'>
                              <MDBIcon fab icon='facebook' />
                            </a>
                          </li>
                          <li className='list-inline-item'>
                            <a href='#!' className='p-2 fa-lg tw-ic'>
                              <MDBIcon fab icon='twitter' />
                            </a>
                          </li>
                          <li className='list-inline-item'>
                            <a href='#!' className='p-2 fa-lg gplus-ic'>
                              <MDBIcon fab icon='google-plus' />
                            </a>
                          </li>
                          <li className='list-inline-item'>
                            <a href='#!' className='p-2 fa-lg li-ic'>
                              <MDBIcon fab icon='linkedin' />
                            </a>
                          </li>
                        </ul>
                        <a
                          href='#!'
                          className='rotate-btn text-dark'
                          data-card='card-1'
                          onClick={this.handleFlipping(1)}
                        >
                          <MDBIcon icon='undo' /> Click here to rotate back
                  </a>
                      </MDBCardBody>
                    </MDBCard>
                  </MDBRotatingCard>
                </MDBCol>
                <MDBCol md="3" style={{ minHeight: '26rem', maxWidth: '22rem' }}>
                  
                  <MDBRotatingCard
                    flipped={flipped2}
                    className='text-center h-100 w-100'
                  >
                    <MDBCard className='face front'>
                      <MDBCardUp>
                        <img
                          className='card-img-top'
                          src='https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/img (56).jpg'
                          alt=''
                        />
                      </MDBCardUp>
                      <MDBAvatar className='mx-auto white' circle>
                        <img
                          src='https://web-complett.de/mdb-react-pro/placeholder/placeholder-men-1-676x676.png'
                          alt=''
                          className='rounded-circle'
                        />
                      </MDBAvatar>
                      <MDBCardBody>
                        <h4 className='font-weight-bold mb-3 blue-text'>John Doe</h4>
                        <p className='font-weight-bold blue-text'>Mobiles Bezahlen</p>
                        <a
                          href='#!'
                          className='rotate-btn text-dark'
                          data-card='card-2'
                          onClick={this.handleFlipping(2)}
                        >
                          <MDBIcon icon='redo' /> Click here to rotate
                  </a>
                      </MDBCardBody>
                    </MDBCard>
                    <MDBCard className='face back' style={{ height: '400px' }}>
                      <MDBCardBody>
                        <h4 className='font-weight-bold black-text'>About me</h4>
                        <hr />
                        <p className='black-text'>
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                          Maxime quae, dolores dicta. Blanditiis rem amet repellat,
                          dolores nihil quae in mollitia asperiores ut rerum
                          repellendus, voluptatum eum, officia laudantium quaerat?
                  </p>
                        <hr />
                        <ul className='list-inline py-2'>
                          <li className='list-inline-item'>
                            <a href='#!' className='p-2 fa-lg fb-ic'>
                              <MDBIcon fab icon='facebook' />
                            </a>
                          </li>
                          <li className='list-inline-item'>
                            <a href='#!' className='p-2 fa-lg tw-ic'>
                              <MDBIcon fab icon='twitter' />
                            </a>
                          </li>
                          <li className='list-inline-item'>
                            <a href='#!' className='p-2 fa-lg gplus-ic'>
                              <MDBIcon fab icon='google-plus' />
                            </a>
                          </li>
                          <li className='list-inline-item'>
                            <a href='#!' className='p-2 fa-lg li-ic'>
                              <MDBIcon fab icon='linkedin' />
                            </a>
                          </li>
                        </ul>
                        <a
                          href='#!'
                          className='rotate-btn text-dark'
                          data-card='card-2'
                          onClick={this.handleFlipping(2)}
                        >
                          <MDBIcon icon='undo' /> Click here to rotate back
                  </a>
                      </MDBCardBody>
                    </MDBCard>
                  </MDBRotatingCard>
                </MDBCol>
                <MDBCol md="3" style={{ minHeight: '26rem', maxWidth: '22rem' }}>
                  
                  <MDBRotatingCard
                    flipped={flipped3}
                    className='text-center h-100 w-100'
                  >
                    <MDBCard className='face front'>
                      <MDBCardUp>
                        <img
                          className='card-img-top'
                          src='https://mdbootstrap.com/img/Photos/Others/project4.jpg'
                          alt=''
                        />
                      </MDBCardUp>
                      <MDBAvatar className='mx-auto white' circle>
                        <img
                          src='https://web-complett.de/mdb-react-pro/placeholder/placeholder-men-1-676x676.png'
                          alt=''
                          className='rounded-circle'
                        />
                      </MDBAvatar>
                      <MDBCardBody>
                        <h4 className='font-weight-bold mb-3 blue-text'>Tom Adams</h4>
                        <p className='font-weight-bold blue-text'>Apple Pay</p>
                        <a
                          href='#!'
                          className='rotate-btn text-dark'
                          data-card='card-3'
                          onClick={this.handleFlipping(3)}
                        >
                          <MDBIcon icon='redo' /> Click here to rotate
                  </a>
                      </MDBCardBody>
                    </MDBCard>
                    <MDBCard className='face back' style={{ height: '400px' }}>
                      <MDBCardBody>
                        <h4 className='font-weight-bold black-text'>About me</h4>
                        <hr />
                        <p className='black-text'>
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                          Maxime quae, dolores dicta. Blanditiis rem amet repellat,
                          dolores nihil quae in mollitia asperiores ut rerum
                          repellendus, voluptatum eum, officia laudantium quaerat?
                  </p>
                        <hr />
                        <ul className='list-inline py-2'>
                          <li className='list-inline-item'>
                            <a href='#!' className='p-2 fa-lg fb-ic'>
                              <MDBIcon fab icon='facebook' />
                            </a>
                          </li>
                          <li className='list-inline-item'>
                            <a href='#!' className='p-2 fa-lg tw-ic'>
                              <MDBIcon fab icon='twitter' />
                            </a>
                          </li>
                          <li className='list-inline-item'>
                            <a href='#!' className='p-2 fa-lg gplus-ic'>
                              <MDBIcon fab icon='google-plus' />
                            </a>
                          </li>
                          <li className='list-inline-item'>
                            <a href='#!' className='p-2 fa-lg li-ic'>
                              <MDBIcon fab icon='linkedin' />
                            </a>
                          </li>
                        </ul>
                        <a
                          href='#!'
                          className='rotate-btn text-dark'
                          data-card='card-3'
                          onClick={this.handleFlipping(3)}
                        >
                          <MDBIcon icon='undo' /> Click here to rotate back
                  </a>
                      </MDBCardBody>
                    </MDBCard>
                  </MDBRotatingCard>
                </MDBCol>
                <MDBCol md="3" style={{ minHeight: '26rem', maxWidth: '22rem' }}>
                  
                  <MDBRotatingCard
                    flipped={flipped4}
                    className='text-center h-100 w-100'
                  >
                    <MDBCard className='face front'>
                      <MDBCardUp>
                        <img
                          className='card-img-top'
                          src='https://mdbootstrap.com/img/Photos/Others/screens-section.jpg'
                          alt=''
                        />
                      </MDBCardUp>
                      <MDBAvatar className='mx-auto white' circle>
                        <img
                          src='https://web-complett.de/mdb-react-pro/placeholder/placeholder-women-7-676x676.png'
                          alt=''
                          className='rounded-circle'
                        />
                      </MDBAvatar>
                      <MDBCardBody>
                        <h4 className='font-weight-bold mb-3 blue-text'>Marie Kate</h4>
                        <p className='font-weight-bold blue-text'>Digitale Beratung per Videochat</p>
                        <a
                          href='#!'
                          className='rotate-btn text-dark'
                          data-card='card-4'
                          onClick={this.handleFlipping(4)}
                        >
                          <MDBIcon icon='redo' /> Click here to rotate
                  </a>
                      </MDBCardBody>
                    </MDBCard>
                    <MDBCard className='face back' style={{ height: '400px' }}>
                      <MDBCardBody>
                        <h4 className='font-weight-bold black-text'>About me</h4>
                        <hr />
                        <p className='black-text'>
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                          Maxime quae, dolores dicta. Blanditiis rem amet repellat,
                          dolores nihil quae in mollitia asperiores ut rerum
                          repellendus, voluptatum eum, officia laudantium quaerat?
                  </p>
                        <hr />
                        <ul className='list-inline py-2'>
                          <li className='list-inline-item'>
                            <a href='#!' className='p-2 fa-lg fb-ic'>
                              <MDBIcon fab icon='facebook' />
                            </a>
                          </li>
                          <li className='list-inline-item'>
                            <a href='#!' className='p-2 fa-lg tw-ic'>
                              <MDBIcon fab icon='twitter' />
                            </a>
                          </li>
                          <li className='list-inline-item'>
                            <a href='#!' className='p-2 fa-lg gplus-ic'>
                              <MDBIcon fab icon='google-plus' />
                            </a>
                          </li>
                          <li className='list-inline-item'>
                            <a href='#!' className='p-2 fa-lg li-ic'>
                              <MDBIcon fab icon='linkedin' />
                            </a>
                          </li>
                        </ul>
                        <a
                          href='#!'
                          className='rotate-btn text-dark'
                          data-card='card-4'
                          onClick={this.handleFlipping(4)}
                        >
                          <MDBIcon icon='undo' /> Click here to rotate back
                  </a>
                      </MDBCardBody>
                    </MDBCard>
                  </MDBRotatingCard>
                </MDBCol>
              </SectionContainer>
                  <h2 className='mt-5 morpheus-den-gradient z-depth-5 white-text p-5 border'>Ihre Finanzen im Überblick</h2>
              <MDBContainer fluid className='mt-5 mb-5 morpheus-den-gradient z-depth-5 white-text p-5 border'>
                <MDBAnimation type='fadeInRight' delay='.3s' header='Tooltip'>
                  
                  {/*  <MDBRow className='py-3'>
                    <MDBCol md='12'>
                      <SectionContainer
                        header='With scrollX and scrollY properties'
                        noBorder
                      >
                        <MDBCard>
                          <MDBCardBody>
                            <MDBDataTable
                              striped
                              bordered
                              hover
                              scrollX
                              scrollY
                              maxHeight='300xp'
                              data='https://my-json-server.typicode.com/Rotarepmi/exjson/db'                              
                            />
                          </MDBCardBody>
                        </MDBCard>
                      </SectionContainer>
                    </MDBCol>
                  </MDBRow>  */}
                  <DashboardCharts />
                </MDBAnimation>
              </MDBContainer>


            </main>
          </MDBView>


        </div>
      </Router>
    );
  }
}

export default DoubleNavigationPage;
