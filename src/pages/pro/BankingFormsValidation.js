import React from "react";
import { MDBRow, MDBCol, MDBBtn, MDBContainer } from "mdbreact";
import { useState } from "react"; 
import { useSpeechSynthesis } from "react-speech-kit";

class BankingFormsValidation extends React.Component {
  state = {
    fname: "Mark",
    lname: "Otto",
    email: "",
    city: "",
    state: "",
    zip: "",
    iban: "",
    bic: "",
    itin: "",
  };

  state = {
    bankleitzahl: 'DE32 1234 5678 9100 4567 22',
    
  };

  submitHandler = event => {
    event.preventDefault();
    event.target.className += " was-validated";
  };

  changeHandler = event => {
    this.setState({ [event.target.name]: event.target.value });
  };


  render() {

    /* const { username, password } = this.state; */

    function Example() {

      const [text] = useState('Sie haben eine falsche IBAN eingegeben oder die IBAN passt nicht zur angegeben BIC. Bitte beachten Sie das die IBAN 22 Zeichen ohne Leerstellen lang sein muss. ');

  const [voiceIndex] = useState(null);
  const onEnd = () => {
    // You could do something here after speaking has finished
  };
  const {
    speak,
    cancel,
    speaking,
    supported,
    voices
  } = useSpeechSynthesis({ onEnd });

  const voice = voices[voiceIndex] || null;

  return (
    <MDBContainer>
    <div>

    { !supported && (
      <p>Oh no, it looks like your browser doesn&#39;t support Speech Synthesis.</p>
    )}
    {supported && (
      
      <React.Fragment>
        
        { speaking
          ? (
            <MDBBtn color="primary" onClick={cancel} size="sm" className="mb-5">
              Stopp
            </MDBBtn>
          ) : (
            <MDBBtn className="morpheus-den-gradient" type="button" onClick={() => speak({ text, voice })} size="sm">
              Info
            </MDBBtn>
          )
        }
      </React.Fragment>
      
    )}
 
    </div>
    </MDBContainer>
  );
}
    
    return (
      <div className="mt-5">
        <form
          className="needs-validation"
          onSubmit={this.submitHandler}
          noValidate
        >
          <MDBRow>
            <MDBCol md="4" className="mb-3">
              <label
                htmlFor="defaultFormRegisterNameEx"
                className="white-text"
              >
                Vorname
              </label>
              <input
                value={this.state.fname}
                name="fname"
                onChange={this.changeHandler}
                type="text"
                id="defaultFormRegisterNameEx"
                className="form-control"
                placeholder="Vorname"
                required
              />
              <div className="valid-tooltip">Validation passed!</div>
            </MDBCol>
            <MDBCol md="4" className="mb-3">
              <label
                htmlFor="defaultFormRegisterEmailEx2"
                className="white-text"
              >
                Nachname
              </label>
              <input
                value={this.state.lname}
                name="lname"
                onChange={this.changeHandler}
                type="text"
                id="defaultFormRegisterEmailEx2"
                className="form-control"
                placeholder="Ihr Nachname"
                required
              />
              <div className="valid-tooltip">Validation passed!</div>
            </MDBCol>
            <MDBCol md="4" className="mb-3">
              <label
                htmlFor="defaultFormRegisterConfirmEx3"
                className="white-text"
              >
                E-Mail
              </label>
              <input
                value={this.state.email}
                onChange={this.changeHandler}
                type="email"
                id="defaultFormRegisterConfirmEx3"
                className="form-control"
                name="email"
                placeholder="Ihre E-Mail Adresse"
              />
            </MDBCol>
          </MDBRow>
          <MDBRow className='mt-3'>
            <MDBCol md="4" className="mb-3">
              <label
                htmlFor="defaultFormRegisterPasswordEx4"
                className="white-text"
              >
                Stadt
              </label>
              <input
                value={this.state.city}
                onChange={this.changeHandler}
                type="text"
                id="defaultFormRegisterPasswordEx4"
                className="form-control"
                name="city"
                placeholder="Stadt"
                required
              />
              <div className="invalid-tooltip">
                Bitte geben Sie eine gültige Stadt an.
              </div>
              <div className="valid-tooltip">Gut!</div>
            </MDBCol>
            <MDBCol md="4" className="mb-3">
              <label
                htmlFor="defaultFormRegisterPasswordEx4"
                className="white-text"
              >
                Bundesland
              </label>
              <input
                value={this.state.state}
                onChange={this.changeHandler}
                type="text"
                id="defaultFormRegisterPasswordEx4"
                className="form-control"
                name="state"
                placeholder="Bundesland"
                required
              />
              <div className="invalid-tooltip">
                Bitte geben Sie ein gültiges Bundesland an.
              </div>
              <div className="valid-tooltip">Looks good!</div>
            </MDBCol>
            <MDBCol md="4" className="mb-3">
              <label
                htmlFor="defaultFormRegisterPasswordEx4"
                className="white-text"
              >
                Zip
              </label>
              <input
                value={this.state.zip}
                onChange={this.changeHandler}
                type="text"
                id="defaultFormRegisterPasswordEx4"
                className="form-control"
                name="zip"
                placeholder="PLZ"
                required
              />
              <div className="invalid-tooltip">
                Bitte geben Sie eine gültige Postleitzahl an.
              </div>
              <div className="valid-tooltip">Gut!</div>
            </MDBCol>
          </MDBRow>
          <MDBRow className='mt-3'>
            <MDBCol md="4" className="mb-3">
              <label
                htmlFor="defaultFormRegisterPasswordEx4"
                className="white-text"
              >
                IBAN
              </label>
              <input
                value={this.state.iban}
                onChange={this.changeHandler}
                type="text"
                id="defaultFormRegisterPasswordEx4"
                className="form-control"
                name="iban"
                placeholder="IBAN"
                required
              />
              <div className="invalid-tooltip">
                Bitte geben Sie eine gültige IBAN an.
              <Example />
              </div>
              <div className="valid-tooltip">Gut!</div>
            </MDBCol>
            <MDBCol md="4" className="mb-3">
              <label
                htmlFor="defaultFormRegisterPasswordEx4"
                className="white-text"
              >
                BIC
              </label>
              <input
                value={this.state.bic}
                onChange={this.changeHandler}
                type="text"
                id="defaultFormRegisterPasswordEx4"
                className="form-control"
                name="bic"
                placeholder="BIC"
                required
              />
              <div className="invalid-tooltip">
                Bitte geben Sie eine gültige BIC an.
              </div>
              <div className="valid-tooltip">Gut!</div>
            </MDBCol>
            <MDBCol md="4" className="mb-3">
              <label
                htmlFor="defaultFormRegisterPasswordEx4"
                className="white-text"
              >
                ITIN
              </label>
              <input
                value={this.state.itin}
                onChange={this.changeHandler}
                type="text"
                id="defaultFormRegisterPasswordEx4"
                className="form-control"
                name="itin"
                placeholder="Steuernummer"
                required
              />
              <div className="invalid-tooltip">
                Bitte geben Sie eine gültige Steuernummer an.
              </div>
              <div className="valid-tooltip">Looks good!</div>
            </MDBCol>
          </MDBRow>
          
          <MDBBtn color="primary" type="submit" className='morpheus-den-gradient mt-5'>
            Abschicken
          </MDBBtn>
        </form>
      </div>
    );
  }
}

export default BankingFormsValidation;