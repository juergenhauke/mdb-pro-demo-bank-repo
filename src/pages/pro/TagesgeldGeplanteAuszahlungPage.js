import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import {
  MDBInput,
  MDBNavbar,
  MDBNavbarNav,
  MDBNavItem,
  MDBNavLink,
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem,
  MDBIcon,
  MDBSideNavCat,
  MDBSideNavNav,
  MDBSideNav,
  MDBContainer,
  MDBAnimation,
  MDBView,
  MDBMask,
  MDBAlert,
  MDBBreadcrumb,
  MDBBreadcrumbItem,
  MDBSmoothScroll,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBDataTable,
  MDBSideNavLink
} from 'mdbreact';
import DocsLink from '../../components/docsLink';
import SectionContainer from '../../components/sectionContainer';


class TagesgeldGeplanteAuszahlungPage extends Component {
  
  state = {
    toggleStateA: false,
    breakWidth: 1300,
    windowWidth: 0
  };

  componentDidMount = () => {
    this.handleResize();
    window.addEventListener('resize', this.handleResize);
  };

  componentWillUnmount = () => {
    window.removeEventListener('resize', this.handleResize);
  };

  handleResize = () =>
    this.setState({
      windowWidth: window.innerWidth
    });

  handleToggleClickA = () => {
    const { toggleStateA } = this.state;
    this.setState({
      toggleStateA: !toggleStateA
    });
  };

  render() {
    
    const { breakWidth, toggleStateA, windowWidth } = this.state;
    const navStyle = {
      paddingLeft: windowWidth > breakWidth ? '210px' : '16px'
    };
    const mainStyle = {
      margin: '0 6%',
      paddingTop: '5.5rem',
      paddingLeft: windowWidth > breakWidth ? '240px' : '0',
    };
    const specialCaseNavbarStyles = {
      WebkitBoxOrient: 'horizontal',
      flexDirection: 'row'
    };
    return (
      <Router>
        <div className='fixed-sn' id='double-page-global'>
          <MDBSideNav
            logo='https://mdbootstrap.com/img/logo/mdb-transparent.png'
            triggerOpening={toggleStateA}
            breakWidth={breakWidth}
            bg='https://mdbootstrap.com/img/Photos/Others/sidenav1.jpg'
            mask='strong'
            fixed
            className='morpheus-den-gradient'
          >
            <li>
              <ul className='social'>
                <li>
                  <a href='#!'>
                    <MDBIcon brand icon='facebook' />
                  </a>
                </li>
                <li>
                  <a href='#!'>
                    <MDBIcon brand icon='pinterest' />
                  </a>
                </li>
                <li>
                  <a href='#!'>
                    <MDBIcon brand icon='google-plus' />
                  </a>
                </li>
                <li>
                  <a href='#!'>
                    <MDBIcon brand icon='twitter' />
                  </a>
                </li>
              </ul>
            </li>
            <MDBInput
              type='text'
              hint='Search'
              style={{
                color: '#fff',
                padding: '8px 10px 8px 30px',
                boxSizing: 'border-box'
              }}
            />
            <MDBSideNavNav>
              <MDBSideNavCat name='Tagesgeld' id='tagesgeld' icon='chevron-right'>
                <MDBSideNavLink to='/navigation/pro/tagesgeld-overview' target='_parent'>Übersicht</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/tagesgeld-auszahlung' target='_parent'>Auszahlung</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/tagesgeld-geplante-auszahlungen' target='_parent'>Geplante Auszahlung</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/tagesgeld-umsaetze-anzeigen' target='_parent'>Umsätze anzeigen</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/tagesgeld-bearbeiten' target='_parent'>Tagesgeld bearbeiten</MDBSideNavLink>
              </MDBSideNavCat>
              <MDBSideNavCat name='Festgeld' id='instruction-cat' icon='hand-pointer'>
                <MDBSideNavLink to='/navigation/pro/festgeld-overview' target='_parent'>Übersicht</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/festgeld-umsaetze-anzeigen' target='_parent'>Umsätze anzeigen</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/festgeld-prolongation' target='_parent'>Prolongation</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/festgeld-bearbeiten' target='_parent'>Festgeld bearbeiten</MDBSideNavLink>
              </MDBSideNavCat>
              <MDBSideNavCat name='Postbox' id='about-cat' icon='eye'>
                <MDBSideNavLink to='/navigation/pro/postbox-neue-nachrichten' target='_parent'>Neue Nachrichten</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/postbox-gelesene-nachrichten' target='_parent'>Gelesene Nachrichten</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/postbox-nachrichten-schreiben' target='_parent'>Nachrichten schreiben</MDBSideNavLink>
              </MDBSideNavCat>
              <MDBSideNavCat name='Service' id='contact-me-cat' icon='envelope'>
                <MDBSideNavLink to='/navigation/pro/service-persoenliche-daten' target='_parent'>Persönliche Daten</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/service-freistellungsauftrag' target='_parent'>Freistellungsauftrag</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/service-einwilligungen' target='_parent'>Einwilligungen</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/service-nichtveranlagungsbescheinigung' target='_parent'>Nichtveranlagungsbescheinigung</MDBSideNavLink>
                <MDBSideNavLink to='/navigation/pro/service-chat' target='_parent'>Hilfe Chat</MDBSideNavLink>
              </MDBSideNavCat>
            </MDBSideNavNav>
          </MDBSideNav>
          <MDBNavbar style={navStyle} double expand='md' fixed='top' scrolling className='morpheus-den-gradient'>
            <MDBNavbarNav left>
              <MDBNavItem>
                <div
                  onClick={this.handleToggleClickA}
                  key='sideNavToggleA'
                  style={{
                    lineHeight: '32px',
                    marginRight: '1em',
                    verticalAlign: 'middle'
                  }}
                >
                  <MDBIcon icon='bars' color='white' size='2x' />
                </div>
              </MDBNavItem>
              <MDBNavItem className='d-none d-md-inline' style={{ paddingTop: 5, color: '#ffffff' }}>
                MyDemoBank
              </MDBNavItem>
            </MDBNavbarNav>
            <MDBNavbarNav right style={specialCaseNavbarStyles}>
              <MDBNavItem active>
                <MDBNavLink to='#!' link>
                  <MDBIcon icon='envelope' className='d-inline-inline' />{' '}
                  <div className='d-none d-md-inline'>Contact</div>
                </MDBNavLink>
              </MDBNavItem>
              <MDBNavItem>
                <MDBNavLink to='#!' link>
                  <MDBIcon icon='comments' className='d-inline-inline' />{' '}
                  <div className='d-none d-md-inline'>Support</div>
                </MDBNavLink>
              </MDBNavItem>
              <MDBNavItem>
                <MDBNavLink to='#!' link>
                  <MDBIcon icon='user' className='d-inline-inline' /> <div className='d-none d-md-inline'>Account</div>
                </MDBNavLink>
              </MDBNavItem>
              <MDBNavItem>
                <MDBDropdown>
                  <MDBDropdownToggle nav caret>
                    <div className='d-none d-md-inline'>Dropdown</div>
                  </MDBDropdownToggle>
                  <MDBDropdownMenu right>
                    <MDBDropdownItem href='#!'>Action</MDBDropdownItem>
                    <MDBDropdownItem href='#!'>Another Action</MDBDropdownItem>
                    <MDBDropdownItem href='#!'>Something else here</MDBDropdownItem>
                    <MDBDropdownItem href='#!'>Something else here</MDBDropdownItem>
                  </MDBDropdownMenu>
                </MDBDropdown>
              </MDBNavItem>
            </MDBNavbarNav>
          </MDBNavbar>
          <MDBView id='page-top'>
            <MDBSmoothScroll
              size='lg'
              fixed
              floating
              className='morpheus-den-gradient'
              to='page-top'
              smooth
            >
              <MDBIcon icon='angle-up' />
            </MDBSmoothScroll>
            <MDBMask className='white-text gradient' />
            <main style={mainStyle}>

              <MDBContainer>
                <MDBAlert color="warning" dismiss>

                  <DocsLink
                    title='Documentation Confluence'
                    href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/48627738/Scripts'
                  />
          Link zum <a href="https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/48627738/Scripts" target="_blank" rel="noopener noreferrer" className="alert-link">Jira Ticket</a>.

                  </MDBAlert>
              </MDBContainer>
              

              <MDBContainer fluid className='mt-5 mb-5 morpheus-den-gradient z-depth-5 white-text p-5 border'>
                <MDBAnimation type='fadeInRight' delay='.3s' header='Tooltip'>
                  <MDBBreadcrumb className='morpheus-den-gradient'>
                    <MDBBreadcrumbItem>Tagesgeld</MDBBreadcrumbItem>
                    <MDBBreadcrumbItem active>Geplante Auszahlungen</MDBBreadcrumbItem>
                  </MDBBreadcrumb>
                  <h2>Tagesgeld - Geplante Auszahlungen</h2>
                  <MDBRow className='py-3'>
                    <MDBCol md='12'>
                      <SectionContainer
                        header='With scrollX and scrollY properties'
                        noBorder
                      >
                        <MDBCard>
                          <MDBCardBody>
                            <MDBDataTable
                              striped
                              bordered
                              hover
                              scrollX
                              scrollY
                              maxHeight='300xp'
                              data='https://my-json-server.typicode.com/Rotarepmi/exjson/db'                              
                            />
                          </MDBCardBody>
                        </MDBCard>
                      </SectionContainer>
                    </MDBCol>
                  </MDBRow>
                  
                </MDBAnimation>
              </MDBContainer>


            </main>
          </MDBView>


        </div>
      </Router>
    );
  }
}

export default TagesgeldGeplanteAuszahlungPage;
