import React, { Component, useState } from 'react';
import { useSpeechRecognition } from "react-speech-kit";
import {
  MDBContainer,
  MDBCard,
  MDBCardBody,
  MDBRow,
  MDBCol,
  MDBListGroup,
  MDBListGroupItem,
  MDBAvatar,
  MDBBadge,
  MDBIcon,
  MDBBtn,
  MDBScrollbar,
  MDBAnimation
} from 'mdbreact';
import SectionContainer from '../../components/sectionContainer';
import './ChatPage.css';

class ChatPage extends Component {
  state = {
    
    messages: [
      {
        author: 'Brad Pitt',
        avatar: 'https://web-complett.de/mdb-react-pro/placeholder/placeholder-women-676x676.png',
        when: '12 mins ago',
        message:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
      },
      {
        author: 'Lara Croft',
        avatar: 'https://mdbootstrap.com/img/Photos/Avatars/avatar-5.jpg',
        when: '13 mins ago',
        message:
          ' Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.'
      },
      {
        author: 'Brad Pitt',
        avatar: 'https://mdbootstrap.com/img/Photos/Avatars/avatar-6.jpg',
        when: '14 mins ago',
        message:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
      }
    ],
    friendsToScroll: [
      {
        name: 'Maria Pednarzki',
        avatar: 'https://mdbootstrap.com/img/Photos/Avatars/avatar-5.jpg',
        message: 'Wie kann ich Ihnen helfen?',
        when: 'Gerade eben',
        toRespond: 1,
        seen: false,
        active: true
      },
      {
        name: 'Jürgen Hauke',
        message: 'Lorem ipsum dolor sit',
        avatar: 'https://web-complett.de/mdb-react-pro/personal/ich-kopf-200x200.png',
        when: '5 min ago',
        toRespond: 0,
        seen: false,
        active: false
      },
      {
        name: 'Maria Smith',
        message: 'Lorem ipsum dolor sit',
        avatar: 'https://web-complett.de/mdb-react-pro/placeholder/placeholder-women-2-676x676.png',
        when: 'Vor 2 Tagen',
        toRespond: 0,
        seen: false,
        active: false
      },
      {
        name: 'Sarah Meljay',
        message: 'Lorem ipsum dolor sit',
        avatar: 'https://web-complett.de/mdb-react-pro/placeholder/placeholder-women-8-676x676.png',
        when: 'am 27.03.2020',
        toRespond: 0,
        seen: false,
        active: false
      },
      {
        name: 'Kate Moss',
        message: 'Lorem ipsum dolor sit',
        avatar: 'https://web-complett.de/mdb-react-pro/placeholder/placeholder-women-3-676x676.png',
        when: 'am 21.01.2020',
        toRespond: 0,
        seen: true,
        active: false
      },
      {
        name: 'Lara Croft',
        message: 'Lorem ipsum dolor sit',
        avatar: 'https://web-complett.de/mdb-react-pro/placeholder/placeholder-women-4-676x676.png',
        when: 'vor einem Jahr',
        toRespond: 0,
        seen: false,
        active: false
      },
      {
        name: 'Brad Pitt',
        message: 'Lorem ipsum dolor sit',
        avatar: 'https://web-complett.de/mdb-react-pro/placeholder/placeholder-women-7-676x676.png',
        when: 'Vor 5 Jahren',
        toRespond: 0,
        seen: true,
        active: false
      },
      {
        name: 'John Doeno',
        avatar: 'https://mdbootstrap.com/img/Photos/Avatars/avatar-8.jpg',
        message: 'Hello, Are you there?',
        when: 'Vor 10 Jahren',
        toRespond: 1,
        seen: false,
        active: false
      },
      {
        name: 'Danny Smithy',
        message: 'Lorem ipsum dolor sit',
        avatar: 'https://mdbootstrap.com/img/Photos/Avatars/avatar-1.jpg',
        when: '5 min ago',
        toRespond: 0,
        seen: false,
        active: false
      },
      {
        name: 'Alex Stewardo',
        message: 'Lorem ipsum dolor sit',
        avatar: 'https://mdbootstrap.com/img/Photos/Avatars/avatar-2.jpg',
        when: 'Yesterday',
        toRespond: 0,
        seen: false,
        active: false
      },
      {
        name: 'Marta Olsen',
        message: 'Lorem ipsum dolor sit',
        avatar: 'https://mdbootstrap.com/img/Photos/Avatars/avatar-3.jpg',
        when: 'Yesterday',
        toRespond: 0,
        seen: false,
        active: false
      },
      {
        name: 'Kate Boss',
        message: 'Lorem ipsum dolor sit',
        avatar: 'https://mdbootstrap.com/img/Photos/Avatars/avatar-4.jpg',
        when: 'Yesterday',
        toRespond: 0,
        seen: false,
        active: false
      },
      {
        name: 'Tomb Rider',
        message: 'Lorem ipsum dolor sit',
        avatar: 'https://mdbootstrap.com/img/Photos/Avatars/avatar-5.jpg',
        when: 'Yesterday',
        toRespond: 0,
        seen: false,
        active: false
      },
      {
        name: 'Bradley Scott',
        message: 'Lorem ipsum dolor sit',
        avatar: 'https://mdbootstrap.com/img/Photos/Avatars/avatar-6.jpg',
        when: '5 min ago',
        toRespond: 0,
        seen: true,
        active: false
      }
    ],
    messagesToScroll: [
      {
        author: 'Maria Pednarzki',
        avatar: 'https://mdbootstrap.com/img/Photos/Avatars/avatar-5.jpg',
        when: '12 mins ago',
        message:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
      },
      {
        author: 'Jürgen Hauke',
        avatar: 'https://web-complett.de/mdb-react-pro/personal/ich-kopf-200x200.png',
        when: '13 mins ago',
        message:
          ' Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.'
      },
      {
        author: 'Maria Pednarzki',
        avatar: 'https://mdbootstrap.com/img/Photos/Avatars/avatar-5.jpg',
        when: '14 mins ago',
        message:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
      },
      {
        author: 'Jürgen Hauke',
        avatar: 'https://web-complett.de/mdb-react-pro/personal/ich-kopf-200x200.png',
        when: '15 mins ago',
        message:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
      },
      {
        author: 'Maria Pednarzki',
        avatar: 'https://mdbootstrap.com/img/Photos/Avatars/avatar-5.jpg',
        when: '16 mins ago',
        message:
          ' Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.'
      },
      {
        author: 'Jürgen Hauke',
        avatar: 'https://web-complett.de/mdb-react-pro/personal/ich-kopf-200x200.png',
        when: '17 mins ago',
        message:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
      }
    ]
  };

  componentDidMount() {
    this.scrollToLast('.chat-message');
  }

  scrollToLast = selector => {
    const message = document.querySelectorAll(selector);
    message[message.length - 1].scrollIntoView();
  };

  render() {
    const { messagesToScroll, friendsToScroll } = this.state;
    function Example() {
      const [lang] = useState('en-AU');
      const [value, setValue] = useState('');
      const [blocked, setBlocked] = useState(false);

      const onEnd = () => {
        // You could do something here after listening has finished
      };

      const onResult = (result) => {
        setValue(result);
      };

      

      const onError = (event) => {
        if (event.error === 'not-allowed') {
          setBlocked(true);
        }
      };

      const {
        listen,
        listening,
        stop,
        supported
      } = useSpeechRecognition({ onResult, onEnd, onError });

      const toggle = listening
        ? stop
        : () => {
          setBlocked(false);
          listen({ lang });
        };

      return (
        <form id="speech-recognition-form">

          {!supported && (
            <p>Oh no, it looks like your browser doesn&#39;t support Speech Recognition.</p>
          )}
          {supported && (
            <React.Fragment>

              <textarea
                id="transcript"
                name="transcript"
                placeholder="Ich warte auf deine Spracheingabe ..."
                value={value}
                rows={3}
                disabled
                className="black-text"
                style={{ background: '#ffffff' }}
              />
              <MDBBtn className="morpheus-den-gradient" disabled={blocked} type="button" onClick={toggle} style={{ float: 'right' }}>
                {listening ? 'Stopp' : 'Sprechen'}
              </MDBBtn>
              {blocked && <p style={{ color: 'red' }}>The microphone is blocked for this site in your browser.</p>}
            </React.Fragment>
          )}
        </form>
      );
    }


    return (
      <MDBContainer>


        <MDBContainer fluid className='mt-5 mb-5   white-text p-5 '>
          <MDBAnimation type='fadeInRight' delay='.3s' header='Tooltip'>
            <SectionContainer header='MyDemoBank - Helpcenter - Chat'>
              <MDBCard className=' chat-room'>
                <MDBCardBody>
                  <MDBRow className='px-lg-2 px-2'>
                    <MDBCol
                      md='6'
                      xl='4'
                      className='px-0 mb-4 mb-md-0 scrollable-friends-list'
                    >
                      <h6 className='font-weight-bold mb-3 text-lg-left'>Ihre letzten Ansprechpartner</h6>
                      <MDBScrollbar className='white z-depth-1 p-3'>
                        <MDBListGroup className='friend-list'>
                          {friendsToScroll.map(friend => (
                            <Friend key={friend.name} friend={friend} />
                          ))}
                        </MDBListGroup>
                      </MDBScrollbar>
                    </MDBCol>
                    <MDBCol
                      md='6'
                      xl='8'
                      className='pl-md-3 mt-4 mt-md-0 px-lg-auto'
                    >
                      <MDBScrollbar className='scrollable-chat'>
                        <MDBListGroup className='list-unstyled px-3'>
                          {messagesToScroll.map(message => (
                            <ChatMessage
                              key={message.author + message.when}
                              message={message}
                            />
                          ))}
                        </MDBListGroup>
                      </MDBScrollbar>

                      <div className='form-group basic-textarea'>
                        {/* <textarea
                      className='form-control pl-2 my-0'
                      id='exampleFormControlTextarea2'
                      rows='3'
                      placeholder='Type your message here...'
                    /> */}
                        {/* <MDBBtn
                      color='info'
                      rounded
                      size='sm'
                      className='float-right mt-4'
                    >
                      Send
                    </MDBBtn> */}
                        <MDBCard>
                          <MDBCardBody>
                            <div className='d-flex justify-content-between'>
                              <strong className='indigo-text'>Jürgen</strong>
                              <small className='pull-right text-muted'>
                                <MDBIcon icon='clock' />
                              </small>
                            </div>
                            <hr />
                            <Example />
                          </MDBCardBody>
                        </MDBCard>

                      </div>
                    </MDBCol>
                  </MDBRow>
                </MDBCardBody>
              </MDBCard>
            </SectionContainer>
          </MDBAnimation>
        </MDBContainer>


      </MDBContainer>
    );
  }
}

const Friend = ({
  friend: { name, avatar, message, when, toRespond, seen, active }
}) => (
    <MDBListGroupItem
      href='#!'
      className='d-flex justify-content-between p-2 border-light'
      style={{ backgroundColor: active ? '#eeeeee' : '' }}
    >
      <MDBAvatar
        tag='img'
        src={avatar}
        alt='avatar'
        circle
        className='mr-2 z-depth-1'
      />
      <div style={{ fontSize: '0.95rem' }}>
        <strong>{name}</strong>
        <p className='text-muted'>{message}</p>
      </div>
      <div>
        <p className='text-muted mb-0' style={{ fontSize: '0.75rem' }}>
          {when}
        </p>
        {seen ? (
          <span className='text-muted float-right'>
            <MDBIcon icon='check' aria-hidden='true' />
          </span>
        ) : toRespond ? (
          <MDBBadge color='danger' className='float-right'>
            {toRespond}
          </MDBBadge>
        ) : (
              <span className='text-muted float-right'>
                <MDBIcon icon='reply' aria-hidden='true' />
              </span>
            )}
      </div>
    </MDBListGroupItem>
  );

const ChatMessage = ({ message: { author, avatar, when, message } }) => (
  <li className='chat-message d-flex justify-content-between mb-4'>
    <MDBAvatar
      tag='img'
      src={avatar}
      alt='avatar'
      circle
      className='mx-2 z-depth-1'
    />
    <MDBCard>
      <MDBCardBody>
        <div className='d-flex justify-content-between'>
          <strong className='indigo-text'>{author}</strong>
          <small className='pull-right text-muted'>
            <MDBIcon icon='clock' /> {when}
          </small>
        </div>
        <hr />
        <p className='mb-0 grey-text'>{message}</p>
      </MDBCardBody>
    </MDBCard>
  </li>
);

export default ChatPage;
