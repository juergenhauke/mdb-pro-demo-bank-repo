import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import {
  MDBEdgeHeader,
  MDBSideNavCat,
  MDBSideNavNav,
  MDBSideNav,
  MDBSideNavLink,
  MDBFreeBird,
  MDBCardBody,
  MDBCard,
  MDBCardTitle,
  MDBCardImage,
  MDBCardText,
  MDBContainer,
  MDBCol,
  MDBRow,
  MDBAnimation,
  MDBIcon,
  MDBBtn,
  MDBNavLink,
  MDBSmoothScroll
  
} from 'mdbreact';
import DocsLink from '../../components/docsLink';
import SectionContainer from '../../components/sectionContainer';

class SideNavPage extends Component {
  state = {
    sideNavLeft: false,
    sideNavRight: false
  };

  sidenavToggle = sidenavId => () => {
    const sidenavNr = `sideNav${sidenavId}`;
    this.setState({
      [sidenavNr]: !this.state[sidenavNr]
    });
  };

  render() {
    const { sideNavRight, sideNavLeft } = this.state;

    return (
      <Router>
        <MDBSmoothScroll
            size='lg'
            fixed
            floating
            className='morpheus-den-gradient'
            to='page-top'
            smooth
          >
            <MDBIcon icon='angle-up' />
          </MDBSmoothScroll>
        <MDBEdgeHeader color='indigo darken-3' className='sectionPage align-items-stretch' style={{ height: '260px' }} id='page-top'/>
        <div className="d-flex justify-content-between">
          <div className="p-2 text-left" style={{marginTop: '-50px'}}>
            <MDBBtn onClick={this.sidenavToggle('Left') } className='morpheus-den-gradient'>
              <MDBIcon size='lg' icon='bars' />
            </MDBBtn>
            {/* the left SideNav: */}
            <MDBSideNav
              
              triggerOpening={sideNavLeft}
              breakWidth={1300}
              className='morpheus-den-gradient'
            >
              <li>
                <div className='logo-wrapper sn-ad-avatar-wrapper'>
                  <a href='#!'>
                    <img
                      alt=''
                      src='https://web-complett.de/mdb-react-pro/personal/ich-kopf-200x200.png'
                      className='rounded-circle'
                    />
                    <span>Documentations</span>
                  </a>
                </div>
              </li>

              <MDBSideNavNav>
                <MDBSideNavLink to='/' topLevel>
                  <MDBIcon icon='home' className='mr-2' />
                  Home
                </MDBSideNavLink>
                
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/60031069/Form+Builder+-+React-Hooks' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Form Generator</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64389123/Speech+-+Api' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Speech Api</a>
                <MDBSideNavCat
                  name='Setup'
                  id='setup'
                  icon='chevron-right'
                >
                  <MDBSideNavLink to='/project-setup' target="_parent">Project Setup</MDBSideNavLink>
                  <MDBSideNavLink to='/scripts' target="_parent">Start / Build / Test</MDBSideNavLink>
                  <MDBSideNavLink to='/dependencies' target="_parent">Dependencies</MDBSideNavLink>
                  <MDBSideNavLink to='/webpack' target="_parent">Webpack</MDBSideNavLink>
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='React'
                  id='react'
                  icon='hand-pointer'
                  href='#'
                >
                  <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64356392/Create+React+App' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Create React App</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64356399/React+Router' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> React Router</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421963/ServiceWorker' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> ServiceWorker</a>
                </MDBSideNavCat>
                <MDBSideNavCat name='Design' id='about' icon='eye'>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421898/MDBootstrap' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> MDBootstrap</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64356365/Colormanagement' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Color-Management</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421915/Fontmanagement' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Font-Management</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421922/Templates' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Templates</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421929/Responsive' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Responsive</a>
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='Contact me'
                  id='contact-me'
                  icon='envelope'
                >
                  <MDBSideNavLink to='/sections/rws-contact' target="_parent">Contact</MDBSideNavLink>
                </MDBSideNavCat>
              </MDBSideNavNav>
            </MDBSideNav>
          </div>
          <div className="p-2 text-left">Flex item 2</div>
          <div className="p-2 text-left" style={{marginTop: '-50px'}}>
            <MDBBtn onClick={this.sidenavToggle('Right')} className='morpheus-den-gradient'>
              <MDBIcon size='lg' icon='bars' />
            </MDBBtn>
            {/* the right SideNav: */}
            <MDBSideNav
             
              triggerOpening={sideNavRight}
              className='morpheus-den-gradient'
              right
              breakWidth={1300}
            >
              <li>
                <div className='logo-wrapper sn-ad-avatar-wrapper'>
                  <a href='#!'>
                    <img
                      alt=''
                      src='https://web-complett.de/mdb-react-pro/personal/ich-kopf-200x200.png'
                      className='rounded-circle'
                    />
                    <span>Examples</span>
                  </a>
                </div>
              </li>
              <li>
                <ul className='social'>
                  <li>
                    <MDBIcon brand icon='facebook' />
                  </li>
                  <li>
                    <MDBIcon brand icon='pinterest' />
                  </li>
                  <li>
                    <MDBIcon brand icon='google-plus' />
                  </li>
                  <li>
                    <MDBIcon brand icon='twitter' />
                  </li>
                </ul>
              </li>
              <MDBSideNavNav>
              <MDBSideNavCat
                  name='Demo Sections'
                  id='registration-login--docs'
                  icon='hand-pointer'
                >
                  <MDBSideNavLink to='/sections/app' target='_parent'>App</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/blog' target='_parent'>Blog</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/call-to-action-intro' target='_parent'>Call to action</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/classicform' target='_parent'>Classic form</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/contact' target='_parent'>Contact</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/contactform' target='_parent'>Contact form</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/ecommerce' target='_parent'>Ecommerce</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/features' target='_parent'>Features</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/magazine' target='_parent'>Magazin</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/minimalistic-intro' target='_parent'>Minimalistic intro</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/parallax-intro' target='_parent'>Parallax intro</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/projects' target='_parent'>Projects</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/social' target='_parent'>Social</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/team' target='_parent'>Teams</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/testimonials' target='_parent'>Testimonials</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/videobackground' target='_parent'>Videobackground</MDBSideNavLink>
                </MDBSideNavCat>
              
                <MDBSideNavCat
                  name='Demo Components'
                  id='components-docs'
                  icon='chevron-right'
                >
                  <MDBSideNavLink to='/css' target='_parent'>CSS</MDBSideNavLink>
                  <MDBSideNavLink to='/components' target='_parent'>Components</MDBSideNavLink>
                  <MDBSideNavLink to='/advanced' target='_parent'>Advanced</MDBSideNavLink>
                  <MDBSideNavLink to='/navigation' target='_parent'>Navigation</MDBSideNavLink>
                  <MDBSideNavLink to='/forms' target='_parent'>Forms</MDBSideNavLink>
                  <MDBSideNavLink to='/tables' target='_parent'>Tables</MDBSideNavLink>
                  <MDBSideNavLink to='/modals' target='_parent'>Modal</MDBSideNavLink>
                  <MDBSideNavLink to='/addons' target='_parent'>Addons</MDBSideNavLink>
                  <MDBSideNavLink to='/sections' target='_parent'>Sections</MDBSideNavLink>
                  
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='Demo Plugins'
                  id='components-docs'
                  icon='chevron-right'
                >
                  <MDBSideNavLink to='/plugins/pro/plugins-wysiwyg' target='_parent'>WYSIWYG</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-tableeditor' target='_parent'>Table Editor</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-sortable' target='_parent'>Sortable</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-fullcalendar' target='_parent'>Fullcalendar</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-colorpicker' target='_parent'>Colorpicker</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-fileupload' target='_parent'>Fileupload</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-filter' target='_parent'>Filter</MDBSideNavLink>
                  
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='Demo Forms'
                  id='registration-login--docs'
                  icon='hand-pointer'
                >
                  <MDBSideNavLink>Registration Forms</MDBSideNavLink>
                  <MDBSideNavLink>Login Forms</MDBSideNavLink>
                  <MDBSideNavLink>Registration/Login Forms</MDBSideNavLink>
                  <MDBSideNavLink>Inline Forms</MDBSideNavLink>
                  <MDBSideNavLink>Modal Forms</MDBSideNavLink>
                  <MDBSideNavLink>Vaklidation Forms</MDBSideNavLink>
                </MDBSideNavCat>
                              
              </MDBSideNavNav>
            </MDBSideNav>
          </div>
        </div>
        
        <SectionContainer header='Slim' flexCenter>       
        <div className='mt-5 mb-5'>
          <MDBFreeBird>
            <MDBRow>
              <MDBCol
                md='10'
                className='mx-auto float-none white z-depth-1 py-2 px-2'              
              >
                <MDBCardBody className='text-center'>
                  <h2 className='h2-responsive mb-4'>
                    <strong className='font-weight-bold'>
                      <img
                        src='https://web-complett.de/mdb-react-pro/logo/logo-transparent-250.png'
                        alt='mdbreact-logo'
                        className='pr-2'
                      />
                    </strong>
                  </h2>
                  <MDBRow />
                  <p>React Bootstrap with Material Design</p>
                  <p className='pb-4'>
                  This application shows the actual use of MDB React
                      components in a webpack 4, react environement and showcases for a demo bank. 
                      The project documentation will be found in confluence and task's will be tracked in jira (please follow links below).
                      The project repository code is stored in github and bitbucket and will be updated alike ( please check update dates on github or bitbucket )
                  </p>
                  <MDBRow className='d-flex flex-row justify-content-center row'>
                      <a
                        className='border nav-link border-light rounded mr-1 mx-2 mb-2'
                        href='https://github.com/othello50/mdb-react-pro2'
                        target='_blank'
                        rel='noopener noreferrer'
                      >
                        {/* <MDBIcon icon='graduation-cap' className='mr-2' /> */}
                        <i className="mr-1 fab fa-github"></i>
                        <span className='font-weight-bold'>
                          Github
                      </span>
                      </a>
                      <a
                        className='border nav-link border-light rounded mx-2 mb-2'
                        href='https://bitbucket.org/juergenhauke/mdb-pro-demo-bank-repo/src/master/'
                        target='_blank'
                        rel='noopener noreferrer'
                      >

                        {/* <MDBIcon far icon='fa-bitbucket' className='mr-5' /> */}
                        <i className="mr-1 fab fa-bitbucket"></i>
                        <span className='font-weight-bold'>BitBucket</span>
                      </a>
                      <a
                        className='border nav-link border-light rounded mx-2 mb-2'
                        href='https://webcomplett.atlassian.net/secure/RapidBoard.jspa?rapidView=2&projectKey=MDB&view=planning&selectedIssue=MDB-2&issueLimit=100'
                        target='_blank'
                        rel='noopener noreferrer'
                      >
                        {/* <MDBIcon icon='download' className='mr-2' /> */}
                        <i className="mr-1 fab fa-jira"></i>
                        <span className='font-weight-bold'>Jira</span>
                      </a>
                      <a
                        className='border nav-link border-light rounded mx-2 mb-2'
                        href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/overview'
                        target='_blank'
                        rel='noopener noreferrer'
                      >
                        {/* <MDBIcon icon='download' className='mr-2' /> */}
                        <i className="mr-1 fab fa-confluence"></i>
                        <span className='font-weight-bold'>Confluence</span>
                      </a>
                    </MDBRow>
                </MDBCardBody>
              </MDBCol>
            </MDBRow>
          </MDBFreeBird>
          <MDBContainer>
              <MDBRow>
                <MDBCol md='12' className='mt-4'>
                  <h2 className='text-center my-5 font-weight-bold'>
                    The goal of this project and why i decide to setup this project:
                </h2>
                  <p className='text-center text-muted mb-1'>
                    Google has designed a Material Design to make the web more
                    beautiful and more user-friendly.
                </p>
                  <p className='text-center text-muted mb-1'>
                    Twitter has created a Bootstrap to support you in faster and
                    easier development of responsive and effective websites.
                </p>
                  <p className='text-center text-muted'>
                    Together with React a modern, developement environement and demo bank application will be created with - Material Design for Bootstrap 4 latest.
                </p>
                  <hr className='my-5' />
                  <h2 className='text-center my-5 font-weight-bold'>
                    Usage:
                </h2>
                  <p className='text-center text-muted mb-1'>
                    The <strong>left sideNav</strong> shows inline documentation (project setup, dependencies, webpack config, design, layout, accesibility, font-, colormagement etc.)
                </p>
                  <p className='text-center text-muted mb-1'>
                    The <strong>right sideNav</strong> shows demo bank show,- and use cases.
                </p>
                  <p className='text-center text-muted mb-1'>
                    The <strong>topNav</strong> shows  a selection of MDBReactPro components to use.
                </p>
                  <p className='text-center text-muted mb-1'>
                    Below the components are shown additional in a dynamic way. <br />
                    <MDBIcon icon="angle-double-down" className="indigo-text pt-2" size="4x" />
                    
                </p>

                  <MDBRow id='categories'>
                  <MDBContainer>
                      <DocsLink
                        title='Home'
                        href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/overview'
                      />
                    </MDBContainer>
                  <MDBCol md='4'>
                    <MDBAnimation reveal type='fadeInLeft'>
                      <MDBCard cascade className='my-3 grey lighten-4'>
                        <MDBCardImage
                          cascade
                          className='img-fluid'
                          src='https://web-complett.de/mdb-react-pro/rws/rws-css.png'
                        />
                        <MDBCardBody cascade className='text-center'>
                          <MDBCardTitle className="mt-3">
                            <MDBIcon
                              icon='css3'
                              brand
                              className='indigo-text pr-2'
                            />
                            <strong>CSS</strong>
                          </MDBCardTitle>
                          <MDBCardText>
                            Animations, colours, shadows, skins and many more!
                            Get to know all our css styles in one place.
                          </MDBCardText>
                          <MDBNavLink
                            tag='button'
                            to='/css'
                            target='_parent'
                            color='mdb-color'
                            className='btn btn-outline-rws-color btn-sm rws-button-startpage'
                            onClick={this.scrollToTop}
                          >
                            More
                          </MDBNavLink>
                        </MDBCardBody>
                      </MDBCard>
                    </MDBAnimation>
                  </MDBCol>
                  <MDBCol md='4'>
                    <MDBAnimation reveal type='fadeInDown'>
                      <MDBCard cascade className='my-3 grey lighten-4'>
                        <MDBCardImage
                          cascade
                          className='img-fluid'
                          src='https://web-complett.de/mdb-react-pro/rws/rws-components.png'
                        />
                        <MDBCardBody cascade className='text-center'>
                          <MDBCardTitle className="mt-3">
                            <MDBIcon icon='cubes' className='indigo-text pr-2' />
                            <strong>COMPONENTS</strong>
                          </MDBCardTitle>
                          <MDBCardText>
                            Ready-to-use components that you can use in your
                            applications. Both basic and extended versions!
                          </MDBCardText>
                          <MDBNavLink
                            tag='button'
                            to='/components'
                            target='_parent'
                            color='mdb-color'
                            className='btn btn-outline-rws-color btn-sm rws-button-startpage'
                            onClick={this.scrollToTop}
                          >
                            More
                          </MDBNavLink>
                        </MDBCardBody>
                      </MDBCard>
                    </MDBAnimation>
                  </MDBCol>
                  <MDBCol md='4'>
                    <MDBAnimation reveal type='fadeInRight'>
                      <MDBCard cascade className='my-3 grey lighten-4'>
                        <MDBCardImage
                          cascade
                          className='img-fluid'
                          src='https://web-complett.de/mdb-react-pro/rws/rws-advanced.png'
                        />
                        <MDBCardBody cascade className='text-center'>
                          <MDBCardTitle className="mt-3">
                            <MDBIcon icon='code' className='indigo-text pr-2' />
                            <strong>ADVANCED</strong>
                          </MDBCardTitle>
                          <MDBCardText>
                            Advanced components such as charts, carousels,
                            tooltips and popovers. All in Material Design
                            version.
                          </MDBCardText>

                          <MDBNavLink
                            tag='button'
                            to='/advanced'
                            target='_parent'
                            color='mdb-color'
                            className='btn btn-outline-rws-color btn-sm rws-button-startpage'
                            onClick={this.scrollToTop}
                          >
                            More
                          </MDBNavLink>
                        </MDBCardBody>
                      </MDBCard>
                    </MDBAnimation>
                  </MDBCol>
                  </MDBRow>

                  <MDBRow id='categories'>
                  <MDBCol md='4'>
                    <MDBAnimation reveal type='fadeInLeft'>
                      <MDBCard cascade className='my-3 grey lighten-4'>
                        <MDBCardImage
                          cascade
                          className='img-fluid'
                          src='https://web-complett.de/mdb-react-pro/rws/rws-navigation.png'
                        />
                        <MDBCardBody cascade className='text-center'>
                          <MDBCardTitle className="mt-3">
                            <MDBIcon icon='bars' className='indigo-text pr-2' />
                            <strong>NAVIGATION</strong>
                          </MDBCardTitle>
                          <MDBCardText>
                            Ready-to-use navigation layouts, navbars,
                            breadcrumbs and much more!
                          </MDBCardText>

                          <MDBNavLink
                            tag='button'
                            to='/navigation'
                            target='_parent'
                            color='mdb-color'
                            className='btn btn-outline-rws-color btn-sm rws-button-startpage'
                            onClick={this.scrollToTop}
                          >
                            More
                          </MDBNavLink>
                        </MDBCardBody>
                      </MDBCard>
                    </MDBAnimation>
                  </MDBCol>
                  <MDBCol md='4'>
                    <MDBAnimation reveal type='fadeIn'>
                      <MDBCard cascade className='my-3 grey lighten-4'>
                        <MDBCardImage
                          cascade
                          className='img-fluid'
                          src='https://web-complett.de/mdb-react-pro/rws/rws-forms.png'
                        />
                        <MDBCardBody cascade className='text-center'>
                          <MDBCardTitle className="mt-3">
                            <MDBIcon icon='edit' className='indigo-text pr-2' />
                            <strong>FORMS</strong>
                          </MDBCardTitle>
                          <MDBCardText>
                            Inputselecst, date and time pickers. Everything in
                            one place is ready to use!
                          </MDBCardText>

                          <MDBNavLink
                            tag='button'
                            to='/forms'
                            target='_parent'
                            color='mdb-color'
                            className='btn btn-outline-rws-color btn-sm rws-button-startpage'
                            onClick={this.scrollToTop}
                          >
                            More
                          </MDBNavLink>
                        </MDBCardBody>
                      </MDBCard>
                    </MDBAnimation>
                  </MDBCol>
                  <MDBCol md='4'>
                    <MDBAnimation reveal type='fadeInRight'>
                      <MDBCard cascade className='my-3 grey lighten-4'>
                        <MDBCardImage
                          cascade
                          className='img-fluid'
                          src='https://web-complett.de/mdb-react-pro/rws/rws-tables.png'
                        />
                        <MDBCardBody cascade className='text-center'>
                          <MDBCardTitle className="mt-3">
                            <MDBIcon icon='table' className='indigo-text pr-2' />
                            <strong>TABLES</strong>
                          </MDBCardTitle>
                          <MDBCardText>
                            Basic and advanced tables. Responsive, datatables,
                            with sorting, searching and export to csv.
                          </MDBCardText>

                          <MDBNavLink
                            tag='button'
                            to='/tables'
                            target='_parent'
                            color='mdb-color'
                            className='btn btn-outline-rws-color btn-sm rws-button-startpage'
                            onClick={this.scrollToTop}
                          >
                            More
                          </MDBNavLink>
                        </MDBCardBody>
                      </MDBCard>
                    </MDBAnimation>
                  </MDBCol>
                  </MDBRow>

                  <MDBRow id='categories'>
                  <MDBCol md='4'>
                    <MDBAnimation reveal type='fadeInLeft'>
                      <MDBCard cascade className='my-3 grey lighten-4'>
                        <MDBCardImage
                          cascade
                          className='img-fluid'
                          src='https://web-complett.de/mdb-react-pro/rws/rws-modals.png'
                        />
                        <MDBCardBody cascade className='text-center'>
                          <MDBCardTitle className="mt-3">
                            <MDBIcon
                              icon='window-restore'
                              far
                              className='indigo-text pr-2'
                            />
                            <strong>MODALS</strong>
                          </MDBCardTitle>
                          <MDBCardText>
                            Modals used to display advanced messages to the
                            user. Cookies, logging in, registration and much
                            more.
                          </MDBCardText>

                          <MDBNavLink
                            tag='button'
                            to='/modals'
                            target='_parent'
                            color='mdb-color'
                            className='btn btn-outline-rws-color btn-sm rws-button-startpage'
                            onClick={this.scrollToTop}
                          >
                            More
                          </MDBNavLink>
                        </MDBCardBody>
                      </MDBCard>
                    </MDBAnimation>
                  </MDBCol>
                  <MDBCol md='4'>
                    <MDBAnimation reveal type='fadeInUp'>
                      <MDBCard cascade className='my-3 grey lighten-4'>
                        <MDBCardImage
                          cascade
                          className='img-fluid'
                          src='https://web-complett.de/mdb-react-pro/rws/rws-plugins.png'
                        />
                        <MDBCardBody cascade className='text-center'>
                          <MDBCardTitle className="mt-3">
                            <MDBIcon
                              icon='arrows-alt'
                              className='indigo-text pr-2'
                            />
                            <strong>PLUGINS & ADDONS</strong>
                          </MDBCardTitle>
                          <MDBCardText>
                            Google Maps, Social Buttons, Contact Forms
                            and Steppers. Find out more about extended
                            components.
                          </MDBCardText>

                          <MDBNavLink
                            tag='button'
                            to='/addons'
                            target='_parent'
                            color='mdb-color'
                            className='btn btn-outline-rws-color btn-sm rws-button-startpage'
                            onClick={this.scrollToTop}
                          >
                            More
                          </MDBNavLink>
                        </MDBCardBody>
                      </MDBCard>
                    </MDBAnimation>
                  </MDBCol>
                  {/* PRO-START */}
                  <MDBCol md='4'>
                    <MDBAnimation reveal type='fadeInRight'>
                      <MDBCard cascade className='my-3 grey lighten-4'>
                        <MDBCardImage
                          cascade
                          className='img-fluid'
                          src='https://web-complett.de/mdb-react-pro/rws/rws-sections.png'
                        />
                        <MDBCardBody cascade className='text-center'>
                          <MDBCardTitle className="mt-3">
                            <MDBIcon icon='th' className='indigo-text pr-2' />
                            <strong>SECTIONS</strong>
                          </MDBCardTitle>
                          <MDBCardText>
                            E-commerce, contact, blog and much more sections.
                            All ready to use in seconds.
                          </MDBCardText>

                          <MDBNavLink
                            tag='button'
                            to='/sections'
                            target='_parent'
                            color='mdb-color'
                            className='btn btn-outline-rws-color btn-sm rws-button-startpage'
                            onClick={this.scrollToTop}
                          >
                            More
                          </MDBNavLink>
                        </MDBCardBody>
                      </MDBCard>
                    </MDBAnimation>
                  </MDBCol>
                  {/* PRO-END */}
                  </MDBRow>
                </MDBCol>
              </MDBRow>
            </MDBContainer>
        </div>
      </SectionContainer>
      </Router>
    );
  }
}

export default SideNavPage;
