import React from "react";
import MDBSortable from "mdb-react-sortable";
import {
  MDBBtn,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBCardTitle,
  MDBCardText,
  MDBCol
} from "mdbreact";
import './PluginSortable.css';

const Card = props => {
  return (
    <MDBCol>
      <MDBCard>
        <MDBCardImage
          className="img-fluid"
          src={`https://mdbootstrap.com/img/Photos/Others/images/${[
            props.index + 1
          ]}.jpg`}
          waves
        />
        <MDBCardBody>
          <MDBCardTitle>Card {props.title}</MDBCardTitle>
          <MDBCardText>
            Some quick example text to build on the card title and make up the
            bulk of the card&apos;s content.
          </MDBCardText>
          <MDBBtn href="#">MDBBtn</MDBBtn>
        </MDBCardBody>
      </MDBCard>
    </MDBCol>
  );
};

function App() {
  const items = [
    {
      title: "1"
    },
    {
      title: "2"
    },
    {
      title: "3"
    }
  ];

  const cards = items.map((item, index) => (
    <Card title={item.title} index={index} />
  ));

  return (
    <div>
      <MDBSortable
        axis="xy"
        items={cards}
        listClassName="SortableList"
      />
    </div>
  );
}

export default App;