import React from "react";
import { MDBRow, MDBCol, MDBInput, MDBBtn } from "mdbreact";

class FormsPage extends React.Component {
  state = {
    fname: "Jürgen",
    lname: "Hauke",
    email: "juergen.hauke@synegnio.de",
    city: "",
    state: "",
    zip: ""
  };

  submitHandler = event => {
    event.preventDefault();
    event.target.className += " was-validated";
  };

  changeHandler = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    return (
      <div>
        <form
          className="needs-validation"
          onSubmit={this.submitHandler}
          noValidate
        >
          <MDBRow>
            <MDBCol md="4">
              <MDBInput
                value={this.state.fname}
                name="fname"
                onChange={this.changeHandler}
                type="text"
                id="materialFormRegisterNameEx"
                label="Vorname"
                required
              >
                <div className="valid-tooltip">Looks good!</div>
              </MDBInput>
            </MDBCol>
            <MDBCol md="4">
              <MDBInput
                value={this.state.lname}
                name="lname"
                onChange={this.changeHandler}
                type="text"
                id="materialFormRegisterEmailEx2"
                label="Nachname"
                required
              >
                <div className="valid-tooltip">Looks good!</div>
              </MDBInput>
            </MDBCol>
            <MDBCol md="4">
              <MDBInput
                value={this.state.email}
                onChange={this.changeHandler}
                type="email"
                id="materialFormRegisterConfirmEx3"
                name="email"
                label="Ihre E-mail Adresse"
              >
                <div className="valid-tooltip">Looks good!</div>
              </MDBInput>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol md="4">
              <MDBInput
                value={this.state.city}
                onChange={this.changeHandler}
                type="text"
                id="materialFormRegisterPasswordEx4"
                name="city"
                label="Stadt"
                required
              >
                <div className="invalid-tooltip">
                  Bitte geben Sie eine gültige Stadt an.
                </div>
                <div className="valid-tooltip">Looks good!</div>
              </MDBInput>
            </MDBCol>
            <MDBCol md="4">
              <MDBInput
                value={this.state.state}
                onChange={this.changeHandler}
                type="text"
                id="materialFormRegisterPasswordEx4"
                name="state"
                label="Bundesland"
                required
              >
                <div className="invalid-tooltip">
                  Bitte geben Sie ein gültiges Bundesland an.
                </div>
                <div className="valid-tooltip">Looks good!</div>
              </MDBInput>
            </MDBCol>
            <MDBCol md="4">
              <MDBInput
                value={this.state.zip}
                onChange={this.changeHandler}
                type="text"
                id="materialFormRegisterPasswordEx4"
                name="zip"
                label="Postleitzahl"
                required
              >
                <div className="invalid-tooltip">
                  Bitte geben Sie eine gültige Postleitzahl ein.
                </div>
                <div className="valid-tooltip">Looks good!</div>
              </MDBInput>
            </MDBCol>
          </MDBRow>
          <MDBBtn className="morpheus-den-gradient mt-5" type="submit">
            Abschicken
          </MDBBtn>
        </form>
      </div>
    );
  }
}

export default FormsPage;