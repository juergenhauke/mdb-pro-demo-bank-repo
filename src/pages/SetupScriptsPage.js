import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import {
  MDBRow,
  MDBCol,
  MDBEdgeHeader,
  MDBSideNavCat,
  MDBSideNavNav,
  MDBSideNav,
  MDBSideNavLink,
  MDBContainer,
  MDBIcon,
  MDBAlert,
  MDBCollapse,
  MDBCard,
  MDBCardBody,
  MDBCollapseHeader,
  MDBBtn,
  MDBFreeBird,
  MDBSmoothScroll
} from 'mdbreact';
import DocsLink from '../components/docsLink';
import SectionContainer from '../components/sectionContainer';

import './SetupSetupProjectPage.css';
class SetupScriptsPage extends React.Component {
  state = {
    sideNavLeft: false,
    sideNavRight: false,
    collapseID: 'collapse3'
  };

  toggleCollapse = collapseID => () =>
    this.setState(prevState => ({
      collapseID: prevState.collapseID !== collapseID ? collapseID : ''
    }));

  sidenavToggle = sidenavId => () => {
    const sidenavNr = `sideNav${sidenavId}`;
    this.setState({
      [sidenavNr]: !this.state[sidenavNr]
    });
  };

  scrollToTop = () => window.scrollTo(0, 0);
  render() {
    const { sideNavRight, sideNavLeft } = this.state;
    const { collapseID } = this.state;
    const resetPadding = {
      padding: '0'
    };

    return (
      <Router>
        <MDBSmoothScroll
          size='lg'
          fixed
          floating
          className='morpheus-den-gradient'
          to='page-top'
          smooth
        >
          <MDBIcon icon='angle-up' />
        </MDBSmoothScroll>
        <MDBEdgeHeader color='indigo darken-3' className='sectionPage align-items-stretch' style={{ height: '260px' }} id='page-top' />
        <div className="d-flex justify-content-between">
          <div className="p-2 text-left" style={{ marginTop: '-50px' }}>
            <MDBBtn onClick={this.sidenavToggle('Left')} className='morpheus-den-gradient'>
              <MDBIcon size='lg' icon='bars' />
            </MDBBtn>
            {/* the left SideNav: */}
            <MDBSideNav
              
              triggerOpening={sideNavLeft}
              breakWidth={1300}
              className='morpheus-den-gradient'
            >
              <li>
                <div className='logo-wrapper sn-ad-avatar-wrapper'>
                  <a href='#!'>
                    <img
                      alt=''
                      src='https://web-complett.de/mdb-react-pro/personal/ich-kopf-200x200.png'
                      className='rounded-circle'
                    />
                    <span>Documentations</span>
                  </a>
                </div>
              </li>

              <MDBSideNavNav>
                <MDBSideNavLink to='/' topLevel>
                  <MDBIcon icon='home' className='mr-2' />
                  Home
                </MDBSideNavLink>
                
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/60031069/Form+Builder+-+React-Hooks' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Form Generator</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64389123/Speech+-+Api' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Speech Api</a>
                <MDBSideNavCat
                  name='Setup'
                  id='setup'
                  icon='chevron-right'
                >
                  <MDBSideNavLink to='/project-setup' target="_parent">Project Setup</MDBSideNavLink>
                  <MDBSideNavLink to='/scripts' target="_parent">Start / Build / Test</MDBSideNavLink>
                  <MDBSideNavLink to='/dependencies' target="_parent">Dependencies</MDBSideNavLink>
                  <MDBSideNavLink to='/webpack' target="_parent">Webpack</MDBSideNavLink>
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='React'
                  id='react'
                  icon='hand-pointer'
                  href='#'
                >
                  <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64356392/Create+React+App' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Create React App</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64356399/React+Router' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> React Router</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421963/ServiceWorker' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> ServiceWorker</a>
                </MDBSideNavCat>
                <MDBSideNavCat name='Design' id='about' icon='eye'>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421898/MDBootstrap' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> MDBootstrap</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64356365/Colormanagement' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Color-Management</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421915/Fontmanagement' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Font-Management</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421922/Templates' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Templates</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421929/Responsive' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Responsive</a>
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='Contact me'
                  id='contact-me'
                  icon='envelope'
                >
                  <MDBSideNavLink to='/sections/rws-contact' target="_parent">Contact</MDBSideNavLink>
                </MDBSideNavCat>
              </MDBSideNavNav>
            </MDBSideNav>
          </div>
          <div className="p-2 text-left">Flex item 2</div>
          <div className="p-2 text-left" style={{marginTop: '-50px'}}>
            <MDBBtn onClick={this.sidenavToggle('Right')} className='morpheus-den-gradient'>
              <MDBIcon size='lg' icon='bars' />
            </MDBBtn>
            {/* the right SideNav: */}
            <MDBSideNav
             
              triggerOpening={sideNavRight}
              className='morpheus-den-gradient'
              right
              breakWidth={1300}
            >
              <li>
                <div className='logo-wrapper sn-ad-avatar-wrapper'>
                  <a href='#!'>
                    <img
                      alt=''
                      src='https://web-complett.de/mdb-react-pro/personal/ich-kopf-200x200.png'
                      className='rounded-circle'
                    />
                    <span>Examples</span>
                  </a>
                </div>
              </li>
              <li>
                <ul className='social'>
                  <li>
                    <MDBIcon brand icon='facebook' />
                  </li>
                  <li>
                    <MDBIcon brand icon='pinterest' />
                  </li>
                  <li>
                    <MDBIcon brand icon='google-plus' />
                  </li>
                  <li>
                    <MDBIcon brand icon='twitter' />
                  </li>
                </ul>
              </li>
              <MDBSideNavNav>
              <MDBSideNavCat
                  name='Demo Sections'
                  id='registration-login--docs'
                  icon='hand-pointer'
                >
                  <MDBSideNavLink to='/sections/app' target='_parent'>App</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/blog' target='_parent'>Blog</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/call-to-action-intro' target='_parent'>Call to action</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/classicform' target='_parent'>Classic form</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/contact' target='_parent'>Contact</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/contactform' target='_parent'>Contact form</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/ecommerce' target='_parent'>Ecommerce</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/features' target='_parent'>Features</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/magazine' target='_parent'>Magazin</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/minimalistic-intro' target='_parent'>Minimalistic intro</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/parallax-intro' target='_parent'>Parallax intro</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/projects' target='_parent'>Projects</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/social' target='_parent'>Social</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/team' target='_parent'>Teams</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/testimonials' target='_parent'>Testimonials</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/videobackground' target='_parent'>Videobackground</MDBSideNavLink>
                </MDBSideNavCat>
              
                <MDBSideNavCat
                  name='Demo Components'
                  id='components-docs'
                  icon='chevron-right'
                >
                  <MDBSideNavLink to='/css' target='_parent'>CSS</MDBSideNavLink>
                  <MDBSideNavLink to='/components' target='_parent'>Components</MDBSideNavLink>
                  <MDBSideNavLink to='/advanced' target='_parent'>Advanced</MDBSideNavLink>
                  <MDBSideNavLink to='/navigation' target='_parent'>Navigation</MDBSideNavLink>
                  <MDBSideNavLink to='/forms' target='_parent'>Forms</MDBSideNavLink>
                  <MDBSideNavLink to='/tables' target='_parent'>Tables</MDBSideNavLink>
                  <MDBSideNavLink to='/modals' target='_parent'>Modal</MDBSideNavLink>
                  <MDBSideNavLink to='/addons' target='_parent'>Addons</MDBSideNavLink>
                  <MDBSideNavLink to='/sections' target='_parent'>Sections</MDBSideNavLink>
                  
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='Demo Plugins'
                  id='components-docs'
                  icon='chevron-right'
                >
                  <MDBSideNavLink to='/plugins/pro/plugins-wysiwyg' target='_parent'>WYSIWYG</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-tableeditor' target='_parent'>Table Editor</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-sortable' target='_parent'>Sortable</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-fullcalendar' target='_parent'>Fullcalendar</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-colorpicker' target='_parent'>Colorpicker</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-fileupload' target='_parent'>Fileupload</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-filter' target='_parent'>Filter</MDBSideNavLink>
                  
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='Demo Forms'
                  id='registration-login--docs'
                  icon='hand-pointer'
                >
                  <MDBSideNavLink>Registration Forms</MDBSideNavLink>
                  <MDBSideNavLink>Login Forms</MDBSideNavLink>
                  <MDBSideNavLink>Registration/Login Forms</MDBSideNavLink>
                  <MDBSideNavLink>Inline Forms</MDBSideNavLink>
                  <MDBSideNavLink>Modal Forms</MDBSideNavLink>
                  <MDBSideNavLink>Vaklidation Forms</MDBSideNavLink>
                </MDBSideNavCat>
                              
              </MDBSideNavNav>
            </MDBSideNav>
          </div>
        </div>
        <MDBFreeBird className='mt-2'>
          <MDBRow>
            <MDBCol
              md='10'
              className='mx-auto float-none white z-depth-1 py-2 px-2'
            >
              <MDBCardBody className='text-center'>
                <h2 className='h2-responsive mb-4'>
                  <strong className='font-weight-bold'>
                    <img
                      src='https://web-complett.de/mdb-react-pro/logo/logo-transparent-250.png'
                      alt='mdbreact-logo'
                      className='pr-2'
                    />
                  </strong>
                </h2>
                <MDBRow />
                <p>React Bootstrap with Material Design</p>
                <p className='pb-4'>
                  This application shows the actual use of MDB React
                  components in a webpack 4, react environement and showcases for a demo bank.
                  The project documentation will be found in confluence and task's will be tracked in jira (please follow links below).
                  The project repository code is stored in github and bitbucket and will be updated alike ( please check update dates on github or bitbucket )
                  </p>
                <MDBRow className='d-flex flex-row justify-content-center row'>
                  <a
                    className='border nav-link border-light rounded mr-1 mx-2 mb-2'
                    href='https://github.com/othello50/mdb-react-pro2'
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    {/* <MDBIcon icon='graduation-cap' className='mr-2' /> */}
                    <i className="mr-1 fab fa-github"></i>
                    <span className='font-weight-bold'>
                      Github
                      </span>
                  </a>
                  <a
                    className='border nav-link border-light rounded mx-2 mb-2'
                    href='https://bitbucket.org/juergenhauke/mdb-react-pro2/src/master/'
                    target='_blank'
                    rel='noopener noreferrer'
                  >

                    {/* <MDBIcon far icon='fa-bitbucket' className='mr-5' /> */}
                    <i className="mr-1 fab fa-bitbucket"></i>
                    <span className='font-weight-bold'>BitBucket</span>
                  </a>
                  <a
                    className='border nav-link border-light rounded mx-2 mb-2'
                    href='https://webcomplett.atlassian.net/secure/BrowseProjects.jspa'
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    {/* <MDBIcon icon='download' className='mr-2' /> */}
                    <i className="mr-1 fab fa-jira"></i>
                    <span className='font-weight-bold'>Jira</span>
                  </a>
                  <a
                    className='border nav-link border-light rounded mx-2 mb-2'
                    href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/overview'
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    {/* <MDBIcon icon='download' className='mr-2' /> */}
                    <i className="mr-1 fab fa-confluence"></i>
                    <span className='font-weight-bold'>Confluence</span>
                  </a>
                </MDBRow>
              </MDBCardBody>
            </MDBCol>
          </MDBRow>
        </MDBFreeBird>
        <MDBContainer>
          <DocsLink
            title='Setup | Setup Scripts'
            href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/48627738/Scripts'
          />
          <h4 className="mb-2">Setup Scripts</h4>


          <SectionContainer header='Scripts Usage'>
            <MDBCard
              className='card-image'
              style={{
                backgroundImage: 'url(https://mdbootstrap.com/img/Photos/Horizontal/Work/4-col/img%20%2814%29.jpg)'
              }}
            >
              <div className='rgba-black-strong py-5 px-2'>
                <MDBRow className='d-flex justify-content-center'>
                  <MDBCol md='12' xl='12'>
                    <MDBContainer className='accordion md-accordion accordion-6' style={{ resetPadding }}>
                      <MDBCard className='mb-4'>
                        <MDBCollapseHeader
                          onClick={this.toggleCollapse('collapse1')}
                          className='p-0 z-depth-1'
                          tag='h4'
                          tagClassName='text-uppercase white-text mb-0 d-flex justify-content-start align-items-center'                        
                        >
                          <div
                            className='d-flex justify-content-center align-items-center mr-4'
                            style={{ backgroundColor: '#fff', minWidth: '100px' }}
                          >
                            <MDBIcon icon='cogs' size='2x' className='m-3 black-text' />
                          </div>
                        Start - Script
                      </MDBCollapseHeader>

                        <MDBCollapse id='collapse1' isOpen={collapseID}>
                          <MDBCardBody className='rgba-black-light white-text z-depth-1'>
                            <div className='p-md-4 mb-0'>
                              <div className="list-group-flush">
                                <div className="list-group-item">
                                  <p className="mb-0">
                                    <MDBBtn floating size="2x" color="rws" className="pb-2"><MDBIcon fab icon="yarn" /></MDBBtn>
                                    <span className="h4">
                                      <strong className="indigo-text">Umgebung starten</strong>
                                      <small className="text-muted"> - yarn</small>
                                    </span>
                                  </p>
                                  <div className="badge badge-rws text-wrap mt-2">
                                    <MDBIcon icon="link" className="mr-2" />
                                    <a href="https://yarnpkg.com//" target="_blank" rel="noopener noreferrer">
                                      https://yarnpkg.com/</a>
                                  </div>
                                  <p className="note note-rws-white"><strong>Note: </strong>
                                   Mehr Informationen zum <mark>yarn package manager</mark> gibt es auf der <a href="https://yarnpkg.com/" target="_blank" rel="noopener noreferrer">
                                      <MDBIcon icon="link" className="mr-1" />Projektseite</a>.</p>
                                  <p className="mt-5 note note-code">$ yarn start | -> "start": "node scripts/start.js" <span className="amber-text">Entwicklungsumgebung über yarn starten</span></p>
                                </div>
                                <div className="list-group-item mt-5">
                                  <p className="mb-0">
                                    <MDBBtn floating size="2x" color="rws" className="pb-2"><MDBIcon fab icon="npm" /></MDBBtn>
                                    <span className="h4">
                                      <strong className="indigo-text">Umgebung starten</strong>
                                      <small className="text-muted"> - npm</small>
                                    </span>
                                  </p>
                                  <div className="badge badge-rws text-wrap">
                                    <MDBIcon icon="link" className="mr-2" />
                                    <a href="https://www.npmjs.com/" target="_blank" rel="noopener noreferrer">https://www.npmjs.com/</a>
                                  </div>
                                  <p className="note note-rws-white"><strong>Note: </strong>
                                    Mehr Informationen zum <mark>npm package manager</mark> gibt es auf <a href="https://www.npmjs.com/" target="_blank" rel="noopener noreferrer">
                                      <MDBIcon icon="link" className="mr-1" />Projektseite.</a>.</p>
                                  <p className="mt-5 note note-code">$ npm start | -> "start": "node scripts/start.js" <span className="amber-text">Entwicklungsumgebung über npm starten</span></p>
                                </div>
                                <MDBAlert color="warning" className="mt-3 mb-3">
                                  <MDBIcon icon="exclamation-triangle" size="lg" className="mr-2" />
                                  <strong>Achtung:</strong> Es empfielt sich entweder yarn oder npm zu benutzen um Konflikte in der package.json zu vermeiden.
                                </MDBAlert>
                              </div>
                            </div>
                          </MDBCardBody>
                        </MDBCollapse>
                      </MDBCard>

                      <MDBCard className='mb-4'>
                        <MDBCollapseHeader
                          onClick={this.toggleCollapse('collapse18')}
                          className='p-0 z-depth-1'
                          tag='h4'
                          tagClassName='text-uppercase white-text mb-0 d-flex justify-content-start align-items-center'
                        >
                          <div
                            className='d-flex justify-content-center align-items-center mr-4'
                            style={{ backgroundColor: '#fff', minWidth: '100px' }}
                          >
                            <MDBIcon icon='cogs' size='2x' className='m-3 black-text' />
                          </div>
                        Build Script
                      </MDBCollapseHeader>

                        <MDBCollapse id='collapse18' isOpen={collapseID}>
                          <MDBCardBody className='rgba-black-light white-text z-depth-1'>
                          <div className='p-md-4 mb-0'>
                              <div className="list-group-flush">
                                <div className="list-group-item">
                                  <p className="mb-0">
                                    <MDBBtn floating size="2x" color="rws" className="pb-2"><MDBIcon fab icon="yarn" /></MDBBtn>
                                    <span className="h4">
                                      <strong className="indigo-text">Umgebung bauen</strong>
                                      <small className="text-muted"> - yarn</small>
                                    </span>
                                  </p>
                                  <div className="badge badge-rws text-wrap mt-2">
                                    <MDBIcon icon="link" className="mr-2" />
                                    <a href="https://yarnpkg.com//" target="_blank" rel="noopener noreferrer">
                                      https://yarnpkg.com/</a>
                                  </div>
                                  <p className="note note-rws-white"><strong>Note: </strong>
                                   Mehr Informationen zum <mark>yarn package manager</mark> gibt es auf der <a href="https://yarnpkg.com/" target="_blank" rel="noopener noreferrer">
                                      <MDBIcon icon="link" className="mr-1" />Projektseite</a>.</p>
                                  <p className="mt-5 note note-code">$ yarn run build | -> "build": "node scripts/build.js" <span className="amber-text">Entwicklungsumgebung mit yarn bauen</span></p>
                                </div>
                                <div className="list-group-item mt-5">
                                  <p className="mb-0">
                                    <MDBBtn floating size="2x" color="rws" className="pb-2"><MDBIcon fab icon="npm" /></MDBBtn>
                                    <span className="h4">
                                      <strong className="indigo-text">Umgebung bauen</strong>
                                      <small className="text-muted"> - npm</small>
                                    </span>
                                  </p>
                                  <div className="badge badge-rws text-wrap">
                                    <MDBIcon icon="link" className="mr-2" />
                                    <a href="https://www.npmjs.com/" target="_blank" rel="noopener noreferrer">https://www.npmjs.com/</a>
                                  </div>
                                  <p className="note note-rws-white"><strong>Note: </strong>
                                    Mehr Informationen zum <mark>npm package manager</mark> gibt es auf <a href="https://www.npmjs.com/" target="_blank" rel="noopener noreferrer">
                                      <MDBIcon icon="link" className="mr-1" />Projektseite.</a>.</p>
                                  <p className="mt-5 note note-code">$ npm run build | -> "build": "node scripts/build.js" <span className="amber-text">Entwicklungsumgebung mit npm bauen</span></p>
                                </div>
                                <MDBAlert color="warning" className="mt-3 mb-3">
                                  <MDBIcon icon="exclamation-triangle" size="lg" className="mr-2" />
                                  <strong>Achtung:</strong> Es empfielt sich entweder yarn oder npm zu benutzen um Konflikte in der package.json zu vermeiden.
                                </MDBAlert>
                              </div>
                            </div>
                          </MDBCardBody>
                        </MDBCollapse>
                      </MDBCard>

                      <MDBCard className='mb-4'>
                        <MDBCollapseHeader
                          onClick={this.toggleCollapse('collapse19')}
                          className='p-0 z-depth-1'
                          tag='h4'
                          tagClassName='text-uppercase white-text mb-0 d-flex justify-content-start align-items-center'
                        >
                          <div
                            className='d-flex justify-content-center align-items-center mr-4'
                            style={{ backgroundColor: '#fff', minWidth: '100px' }}
                          >
                            <MDBIcon icon='cogs' size='2x' className='m-3 black-text' />
                          </div>
                        Test Script
                      </MDBCollapseHeader>

                        <MDBCollapse id='collapse19' isOpen={collapseID}>
                          <MDBCardBody className='rgba-black-light white-text z-depth-1'>
                            <div className='p-md-4 mb-0'>
                              <div className="list-group-flush">
                                <div className="list-group-item">
                                  <p className="mb-0">
                                    <MDBBtn floating size="2x" color="rws" className="pb-2"><MDBIcon icon="cog" /></MDBBtn>
                                    <span className="h4">
                                      <strong className="indigo-text">Projekt testen</strong>
                                      <small className="text-muted"> - Testen</small>
                                    </span>
                                  </p>
                                  <p className="note note-rws-white"><strong>Note: </strong>
                                   Das Projekt kann mittels Javascript Testing Framework <mark>jest</mark> javascriptseitig getetstet werden.
                                   </p>
                                  <p className="mt-5 note note-code">
                                    $ yarn test | -> "test": "node scripts/test.js --env=jsdom" <span className="amber-text">Projekt testen (yarn)</span><br /></p>
                                  <p className="mt-5 mb-r indigo-text">oder</p>
                                  <p className="mt-5 note note-code">$ npm test | -> "test": "node scripts/test.js --env=jsdom"<span className="amber-text">Projekt testen (npm)</span></p>
                                  <p className="mt-5 note note-rws-white"><strong>Note: </strong>Umfangreiche Tests müssen geschrieben werden.</p>                                  
                                </div>
                              </div>
                            </div>
                          </MDBCardBody>
                        </MDBCollapse>
                      </MDBCard>                      
                    </MDBContainer>
                  </MDBCol>
                </MDBRow>
              </div>
            </MDBCard>
          </SectionContainer>
        </MDBContainer>


      </Router>
    );
  }
}

export default SetupScriptsPage;
