import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import {
  MDBRow,
  MDBCol,
  MDBEdgeHeader,
  MDBSideNavCat,
  MDBSideNavNav,
  MDBSideNav,
  MDBSideNavLink,
  MDBContainer,
  MDBIcon,
  MDBAlert,
  MDBCollapse,
  MDBCard,
  MDBCardBody,
  MDBCollapseHeader,
  MDBBtn,
  MDBFreeBird,
  MDBSmoothScroll
} from 'mdbreact';
import DocsLink from '../components/docsLink';
import SectionContainer from '../components/sectionContainer';

import './SetupSetupProjectPage.css';
class SetupSetupProjectPage extends React.Component {
  state = {
    sideNavLeft: false,
    sideNavRight: false,
    collapseID: 'collapse3'
  };

  toggleCollapse = collapseID => () =>
    this.setState(prevState => ({
      collapseID: prevState.collapseID !== collapseID ? collapseID : ''
    }));

  sidenavToggle = sidenavId => () => {
    const sidenavNr = `sideNav${sidenavId}`;
    this.setState({
      [sidenavNr]: !this.state[sidenavNr]
    });
  };

  scrollToTop = () => window.scrollTo(0, 0);
  render() {
    const { sideNavRight, sideNavLeft } = this.state;
    const { collapseID } = this.state;
    const resetPadding = {
      padding: '0'
    };

    return (
      <Router>
        <MDBSmoothScroll
          size='lg'
          fixed
          floating
          className='morpheus-den-gradient'
          to='page-top'
          smooth
        >
          <MDBIcon icon='angle-up' />
        </MDBSmoothScroll>
        <MDBEdgeHeader color='indigo darken-3' className='sectionPage align-items-stretch' style={{ height: '260px' }} id='page-top' />
        <div className="d-flex justify-content-between">
          <div className="p-2 text-left" style={{ marginTop: '-50px' }}>
            <MDBBtn onClick={this.sidenavToggle('Left')} className='morpheus-den-gradient'>
              <MDBIcon size='lg' icon='bars' />
            </MDBBtn>
            {/* the left SideNav: */}
            <MDBSideNav
              
              triggerOpening={sideNavLeft}
              breakWidth={1300}
              className='morpheus-den-gradient'
            >
              <li>
                <div className='logo-wrapper sn-ad-avatar-wrapper'>
                  <a href='#!'>
                    <img
                      alt=''
                      src='https://web-complett.de/mdb-react-pro/personal/ich-kopf-200x200.png'
                      className='rounded-circle'
                    />
                    <span>Documentations</span>
                  </a>
                </div>
              </li>

              <MDBSideNavNav>
                <MDBSideNavLink to='/' topLevel>
                  <MDBIcon icon='home' className='mr-2' />
                  Home
                </MDBSideNavLink>
                
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/60031069/Form+Builder+-+React-Hooks' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Form Generator</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64389123/Speech+-+Api' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Speech Api</a>
                <MDBSideNavCat
                  name='Setup'
                  id='setup'
                  icon='chevron-right'
                >
                  <MDBSideNavLink to='/project-setup' target="_parent">Project Setup</MDBSideNavLink>
                  <MDBSideNavLink to='/scripts' target="_parent">Start / Build / Test</MDBSideNavLink>
                  <MDBSideNavLink to='/dependencies' target="_parent">Dependencies</MDBSideNavLink>
                  <MDBSideNavLink to='/webpack' target="_parent">Webpack</MDBSideNavLink>
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='React'
                  id='react'
                  icon='hand-pointer'
                  href='#'
                >
                  <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64356392/Create+React+App' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Create React App</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64356399/React+Router' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> React Router</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421963/ServiceWorker' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> ServiceWorker</a>
                </MDBSideNavCat>
                <MDBSideNavCat name='Design' id='about' icon='eye'>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421898/MDBootstrap' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> MDBootstrap</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64356365/Colormanagement' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Color-Management</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421915/Fontmanagement' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Font-Management</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421922/Templates' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Templates</a>
                <a href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/64421929/Responsive' target='_blank' rel='noreferrer'><i class="fas fa-camera fa-lg"></i> Responsive</a>
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='Contact me'
                  id='contact-me'
                  icon='envelope'
                >
                  <MDBSideNavLink to='/sections/rws-contact' target="_parent">Contact</MDBSideNavLink>
                </MDBSideNavCat>
              </MDBSideNavNav>
            </MDBSideNav>
          </div>
          <div className="p-2 text-left">Flex item 2</div>
          <div className="p-2 text-left" style={{marginTop: '-50px'}}>
            <MDBBtn onClick={this.sidenavToggle('Right')} className='morpheus-den-gradient'>
              <MDBIcon size='lg' icon='bars' />
            </MDBBtn>
            {/* the right SideNav: */}
            <MDBSideNav
             
              triggerOpening={sideNavRight}
              className='morpheus-den-gradient'
              right
              breakWidth={1300}
            >
              <li>
                <div className='logo-wrapper sn-ad-avatar-wrapper'>
                  <a href='#!'>
                    <img
                      alt=''
                      src='https://web-complett.de/mdb-react-pro/personal/ich-kopf-200x200.png'
                      className='rounded-circle'
                    />
                    <span>Examples</span>
                  </a>
                </div>
              </li>
              <li>
                <ul className='social'>
                  <li>
                    <MDBIcon brand icon='facebook' />
                  </li>
                  <li>
                    <MDBIcon brand icon='pinterest' />
                  </li>
                  <li>
                    <MDBIcon brand icon='google-plus' />
                  </li>
                  <li>
                    <MDBIcon brand icon='twitter' />
                  </li>
                </ul>
              </li>
              <MDBSideNavNav>
              <MDBSideNavCat
                  name='Demo Sections'
                  id='registration-login--docs'
                  icon='hand-pointer'
                >
                  <MDBSideNavLink to='/sections/app' target='_parent'>App</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/blog' target='_parent'>Blog</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/call-to-action-intro' target='_parent'>Call to action</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/classicform' target='_parent'>Classic form</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/contact' target='_parent'>Contact</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/contactform' target='_parent'>Contact form</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/ecommerce' target='_parent'>Ecommerce</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/features' target='_parent'>Features</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/magazine' target='_parent'>Magazin</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/minimalistic-intro' target='_parent'>Minimalistic intro</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/parallax-intro' target='_parent'>Parallax intro</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/projects' target='_parent'>Projects</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/social' target='_parent'>Social</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/team' target='_parent'>Teams</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/testimonials' target='_parent'>Testimonials</MDBSideNavLink>
                  <MDBSideNavLink to='/sections/videobackground' target='_parent'>Videobackground</MDBSideNavLink>
                </MDBSideNavCat>
              
                <MDBSideNavCat
                  name='Demo Components'
                  id='components-docs'
                  icon='chevron-right'
                >
                  <MDBSideNavLink to='/css' target='_parent'>CSS</MDBSideNavLink>
                  <MDBSideNavLink to='/components' target='_parent'>Components</MDBSideNavLink>
                  <MDBSideNavLink to='/advanced' target='_parent'>Advanced</MDBSideNavLink>
                  <MDBSideNavLink to='/navigation' target='_parent'>Navigation</MDBSideNavLink>
                  <MDBSideNavLink to='/forms' target='_parent'>Forms</MDBSideNavLink>
                  <MDBSideNavLink to='/tables' target='_parent'>Tables</MDBSideNavLink>
                  <MDBSideNavLink to='/modals' target='_parent'>Modal</MDBSideNavLink>
                  <MDBSideNavLink to='/addons' target='_parent'>Addons</MDBSideNavLink>
                  <MDBSideNavLink to='/sections' target='_parent'>Sections</MDBSideNavLink>
                  
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='Demo Plugins'
                  id='components-docs'
                  icon='chevron-right'
                >
                  <MDBSideNavLink to='/plugins/pro/plugins-wysiwyg' target='_parent'>WYSIWYG</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-tableeditor' target='_parent'>Table Editor</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-sortable' target='_parent'>Sortable</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-fullcalendar' target='_parent'>Fullcalendar</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-colorpicker' target='_parent'>Colorpicker</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-fileupload' target='_parent'>Fileupload</MDBSideNavLink>
                  <MDBSideNavLink to='/plugins/pro/plugins-filter' target='_parent'>Filter</MDBSideNavLink>
                  
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='Demo Forms'
                  id='registration-login--docs'
                  icon='hand-pointer'
                >
                  <MDBSideNavLink>Registration Forms</MDBSideNavLink>
                  <MDBSideNavLink>Login Forms</MDBSideNavLink>
                  <MDBSideNavLink>Registration/Login Forms</MDBSideNavLink>
                  <MDBSideNavLink>Inline Forms</MDBSideNavLink>
                  <MDBSideNavLink>Modal Forms</MDBSideNavLink>
                  <MDBSideNavLink>Vaklidation Forms</MDBSideNavLink>
                </MDBSideNavCat>
                              
              </MDBSideNavNav>
            </MDBSideNav>
          </div>
        </div>
        <MDBFreeBird className='mt-2'>
          <MDBRow>
            <MDBCol
              md='10'
              className='mx-auto float-none white z-depth-1 py-2 px-2'
            >
              <MDBCardBody className='text-center'>
                <h2 className='h2-responsive mb-4'>
                  <strong className='font-weight-bold'>
                    <img
                      src='https://web-complett.de/mdb-react-pro/logo/logo-transparent-250.png'
                      alt='mdbreact-logo'
                      className='pr-2'
                    />
                  </strong>
                </h2>
                <MDBRow />
                <p>React Bootstrap with Material Design</p>
                <p className='pb-4'>
                  This application shows the actual use of MDB React
                  components in a webpack 4, react environement and showcases for a demo bank.
                  The project documentation will be found in confluence and task's will be tracked in jira (please follow links below).
                  The project repository code is stored in github and bitbucket and will be updated alike ( please check update dates on github or bitbucket )
                  </p>
                <MDBRow className='d-flex flex-row justify-content-center row'>
                  <a
                    className='border nav-link border-light rounded mr-1 mx-2 mb-2'
                    href='https://github.com/othello50/mdb-react-pro2'
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    {/* <MDBIcon icon='graduation-cap' className='mr-2' /> */}
                    <i className="mr-1 fab fa-github"></i>
                    <span className='font-weight-bold'>
                      Github
                      </span>
                  </a>
                  <a
                    className='border nav-link border-light rounded mx-2 mb-2'
                    href='https://bitbucket.org/juergenhauke/mdb-react-pro2/src/master/'
                    target='_blank'
                    rel='noopener noreferrer'
                  >

                    {/* <MDBIcon far icon='fa-bitbucket' className='mr-5' /> */}
                    <i className="mr-1 fab fa-bitbucket"></i>
                    <span className='font-weight-bold'>BitBucket</span>
                  </a>
                  <a
                    className='border nav-link border-light rounded mx-2 mb-2'
                    href='https://webcomplett.atlassian.net/secure/BrowseProjects.jspa'
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    {/* <MDBIcon icon='download' className='mr-2' /> */}
                    <i className="mr-1 fab fa-jira"></i>
                    <span className='font-weight-bold'>Jira</span>
                  </a>
                  <a
                    className='border nav-link border-light rounded mx-2 mb-2'
                    href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/overview'
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    {/* <MDBIcon icon='download' className='mr-2' /> */}
                    <i className="mr-1 fab fa-confluence"></i>
                    <span className='font-weight-bold'>Confluence</span>
                  </a>
                </MDBRow>
              </MDBCardBody>
            </MDBCol>
          </MDBRow>
        </MDBFreeBird>
        <MDBContainer>
          <DocsLink
            title='Setup | Setup Project'
            href='https://webcomplett.atlassian.net/wiki/spaces/MDBREACT/pages/7962696/Project+Setup'
          />
          <h4 className="mb-2">Setup Project</h4>


          <SectionContainer header='Installations - Steps'>
            <MDBCard
              className='card-image'
              style={{
                backgroundImage: 'url(https://mdbootstrap.com/img/Photos/Horizontal/Work/4-col/img%20%2814%29.jpg)'
              }}
            >
              <div className='rgba-black-strong py-5 px-2'>
                <MDBRow className='d-flex justify-content-center'>
                  <MDBCol md='12' xl='12'>
                    <MDBContainer className='accordion md-accordion accordion-6' style={{ resetPadding }}>
                      <MDBCard className='mb-4'>
                        <MDBCollapseHeader
                          onClick={this.toggleCollapse('collapse1')}
                          className='p-0 z-depth-1'
                          tag='h4'
                          tagClassName='text-uppercase white-text mb-0 d-flex justify-content-start align-items-center'
                        >
                          <div
                            className='d-flex justify-content-center align-items-center mr-4'
                            style={{ backgroundColor: '#fff', minWidth: '100px' }}
                          >
                            <MDBIcon icon='list-ol' size='2x' className='m-3 black-text' />
                          </div>
                        Prerequisits
                      </MDBCollapseHeader>

                        <MDBCollapse id='collapse1' isOpen={collapseID}>
                          <MDBCardBody className='rgba-black-light white-text z-depth-1'>
                            <div className='p-md-4 mb-0'>
                              <div className="list-group-flush">
                                <div className="list-group-item">
                                  <p className="mb-0">
                                    <MDBBtn floating size="2x" color="rws" className="pb-2"><MDBIcon fab icon="node-js" /></MDBBtn>
                                    <span className="h4">
                                      <strong className="indigo-text">Nodejs</strong>
                                      <small className="text-muted"> - installieren</small>
                                    </span>
                                  </p>
                                  <div className="badge badge-rws text-wrap mt-2">
                                    <MDBIcon icon="link" className="mr-2" />
                                    <a href="https://nodejs.org/en/download/" target="_blank" rel="noopener noreferrer"><MDBIcon icon="link" className="mr-1" />https://nodejs.org/en/download/</a>
                                  </div>
                                  <p className="note note-rws-white"><strong>Note: </strong>
                      Vom obigen Link können die Installer <mark>*.msi(Win)</mark> / <mark>*.pkg(macOS)</mark> heruntergeladen und installiert werden.<br />
                      Weitere Informationen findet man auf der <a href="https://nodejs.org/en/" target="_blank" rel="noopener noreferrer"><MDBIcon icon="link" className="mr-1" />Projektseite</a>.</p>
                                  <p className="mt-5 note note-code">$ nodejs -v // $ node -v | <span className="amber-text">Nodejs Version überprüfen</span></p>
                                </div>
                                <div className="list-group-item mt-5">
                                  <p className="mb-0">
                                    <MDBBtn floating size="2x" color="rws" className="pb-2"><MDBIcon fab icon="git" /></MDBBtn>
                                    <span className="h4">
                                      <strong className="indigo-text">Git</strong>
                                      <small className="text-muted"> - installieren</small>
                                    </span>
                                  </p>
                                  <div className="badge badge-rws text-wrap mt-2">
                                    <MDBIcon icon="link" className="mr-2" />
                                    <a href="https://git-scm.com/download/win" target="_blank" rel="noopener noreferrer"><MDBIcon icon="link" className="mr-1" /> Win: https://git-scm.com/download/win</a>
                                  </div><br />
                                  <div className="badge badge-rws text-wrap">
                                    <MDBIcon icon="link" className="mr-2" />
                                    <a href="https://git-scm.com/download/mac" target="_blank" rel="noopener noreferrer"><MDBIcon icon="link" className="mr-1" /> macOS: https://git-scm.com/download/mac</a>
                                  </div>
                                  <p className="note note-rws-white"><strong>Note: </strong>
                                    <span>Wer lieber mit dem Desktop-Client arbeiten möchte kann Ihn auf </span><span><a href="https://desktop.github.com/" target="_blank" rel="noopener noreferrer"><MDBIcon icon="link" className="mr-1" />Git Desktop</a> herunterladen.<br />
                      Weitere Informationen findet man auf der <a href="https://git-scm.com/" target="_blank" rel="noopener noreferrer"> <MDBIcon icon="link" className="mr-1" />Projektseite</a>.</span></p>
                                  <p className="mt-5 note note-code">$ git -v | <span className="amber-text">Git Version überprüfen</span></p>
                                </div>
                                <div className="list-group-item mt-5">
                                  <p className="mb-0">
                                    <MDBBtn floating size="2x" color="rws" className="pb-2"><MDBIcon fab icon="npm" /></MDBBtn>
                                    <span className="h4">
                                      <strong className="indigo-text">NPM</strong>
                                      <small className="text-muted"> - installieren</small>
                                    </span>
                                  </p>
                                  <div className="badge badge-rws text-wrap mt-2">
                                    <MDBIcon icon="link" className="mr-2" />
                                    <a href="npmjs.com/get-npm" target="_blank" rel="noopener noreferrer">npmjs.com/get-npm</a>
                                  </div>
                                  <p className=" note note-rws-white"><strong>Note: </strong>
                      Mit der Installation von nodejs ist npm bereits enthalten und muss/kann installiert werden. Um sicher zu gehen das npm installiert ist kann man auch hier den Versionscheck in der Console eingeben:</p>
                                  <p className="mt-5 note note-code">$ npm -v  | <span className="amber-text">NPM Version überprüfen</span></p>
                                  <p className="mt-5 note note-rws-white"><strong>Note 2: </strong>
                       Sollte npm nicht installiert sein empfiehlt es sich, wie im übrigen auch für nodejs, den Paketmanager über einen Node Version Manger zu installieren. <br />
                       Weitere Informationen hierfür findet man unter <a href="https://docs.npmjs.com/downloading-and-installing-node-js-and-npm" target="_blank" rel="noopener noreferrer"><MDBIcon icon="link" className="mr-1" />Download and installing node and npm</a>.
                       <br /><br /> Um auf die aktuelle Version über die Commandozeile upzudaten kann man Folgendes dort eingeben: </p>
                                  <p className="mt-5 note note-code">$ npm install npm -g  | <span className="amber-text">NPM Version aktualisieren (Win)</span></p>
                                </div>
                                <div className="mt-5 mb-5 white-text">oder alternativ:</div>
                                <div className="list-group-item mt-5">
                                  <p className="mb-0">
                                    <MDBBtn floating size="2x" color="rws" className="pb-2"><MDBIcon fab icon="yarn" /></MDBBtn>
                                    <span className="h4">
                                      <strong className="indigo-text">Yarn</strong>
                                      <small className="text-muted"> - installieren</small>
                                    </span>
                                  </p>
                                  <div className="badge badge-rws text-wrap mt-2">
                                    <MDBIcon icon="link" className="mr-2" />
                                    <a href="https://classic.yarnpkg.com/en/docs/install#windows-stable" target="_blank" rel="noopener noreferrer"><MDBIcon icon="link" className="mr-1" />https://classic.yarnpkg.com/en/docs/install#windows-stable</a>
                                  </div>
                                  <p className="note note-rws-white"><strong>Note: </strong>
                      Vom obigen Link können die Installer <mark>*.msi(Win)</mark> / <mark>*.pkg(macOS)</mark> heruntergeladen und installiert werden.<br />
                      Weitere Informationen und Pre-Install Optionen findet man auf <a href="https://classic.yarnpkg.com/" target="_blank" rel="noopener noreferrer"><MDBIcon icon="link" className="mr-1" />https://classic.yarnpkg.com/</a></p>
                                  <p className="mt-5 note note-code">$ yarn --version | <span className="amber-text">Yarn Version überprüfen</span></p>
                                  <p className="mt-5 note note-rws-white"><strong>Note 2: </strong>
                      Um Yarn im Projekt mit allen nötigen Dependencies zu installieren kann man in der Kommandozeile folgendes eingeben:</p>
                                  <p className="mt-5 note note-code">$ yarn |<span className="amber-text"> Yarn installieren</span></p>
                                </div>
                                <MDBAlert color="warning" className="mt-3 mb-3">
                                  <MDBIcon icon="exclamation-triangle" size="lg" className="mr-2" />
                                  <strong>Achtung:</strong> Es empfielt sich entweder yarn oder npm zu benutzen um Konflikte in der package.json zu vermeiden.
                      </MDBAlert>
                              </div>
                            </div>
                          </MDBCardBody>
                        </MDBCollapse>
                      </MDBCard>

                      <MDBCard className='mb-4'>
                        <MDBCollapseHeader
                          onClick={this.toggleCollapse('collapse18')}
                          className='p-0 z-depth-1'
                          tag='h4'
                          tagClassName='text-uppercase white-text mb-0 d-flex justify-content-start align-items-center'
                        >
                          <div
                            className='d-flex justify-content-center align-items-center mr-4'
                            style={{ backgroundColor: '#fff', minWidth: '100px' }}
                          >
                            <MDBIcon icon='terminal' size='2x' className='m-3 black-text' />
                          </div>
                        Installieren / Clonen
                      </MDBCollapseHeader>

                        <MDBCollapse id='collapse18' isOpen={collapseID}>
                          <MDBCardBody className='rgba-black-light white-text z-depth-1'>
                            <div className='p-md-4 mb-0'>
                              <div className="list-group-flush">
                                <div className="list-group-item">
                                  <p className="mb-0">
                                    <MDBBtn floating size="2x" color="rws" className="pb-2"><MDBIcon fab icon="bitbucket" /></MDBBtn>
                                    <span className="h4">
                                      <strong className="indigo-text">Über Bitbucket</strong>
                                      <small className="text-muted"> - installieren / clonen</small>
                                    </span>
                                  </p>
                                  <div className="badge badge-rws text-wrap mt-2">
                                    <MDBIcon icon="link" className="mr-2" />
                                    <a href="https://bitbucket.org/juergenhauke/mdb-react-pro2/src/master/" target="_blank" rel="noopener noreferrer">https://bitbucket.org/juergenhauke/mdb-react-pro2/src/master/</a>
                                  </div>
                                  <p className="note note-rws-white"><strong>Note: </strong>
                      Die Kommandozeile (cmd) öffnen und folgende Befehle (benutzerabhängig) eingeben:</p>
                                  <p className="mt-5 note note-code">
                                    $ cd.. | $ cd meinWorkspace | <span className="amber-text">In Workspace-Ordner wechseln z.B.: C:\projects</span><br />
                        $ mkdir meinProjektordner | <span className="amber-text">Projekt-Ordner anlegen (falls gewünscht)</span><br />
                        $ cd meinProjektordner | <span className="amber-text">In Projekt-Ordner wechseln</span><br />
                        $ git clone https://juergenhauke@bitbucket.org/juergenhauke/mdb-react-pro2.git | <span className="amber-text">Projekt clonen (git)</span></p>
                                  <p className="mt-5 mb-r indigo-text">oder</p>
                                  <p className="mt-5 note note-code">$ yarn add https://juergenhauke@bitbucket.org/juergenhauke/mdb-react-pro2.git | <span className="amber-text">Projekt clonen (yarn)</span></p>
                                </div>
                                <div className="list-group-item mt-5">

                                  <p className="mb-0">
                                    <MDBBtn floating size="lg" color="rws"><MDBIcon fab icon="github" /></MDBBtn>
                                    <span className="h4">
                                      <strong className="indigo-text">Über Github</strong>
                                      <small className="text-muted"> - installieren / clonen</small>
                                    </span>
                                  </p>
                                  <div className="badge badge-rws text-wrap mt-2">
                                    <MDBIcon icon="link" className="mr-2" />
                                    <a href="https://github.com/othello50/mdb-react-pro2" target="_blank" rel="noopener noreferrer">https://github.com/othello50/mdb-react-pro2</a>
                                  </div><br />
                                  <p className="note note-rws-white"><strong>Note: </strong>
                      Die Kommandozeile (cmd) öffnen und folgende Befehle (benutzerabhängig) eingeben:</p>
                                  <p className="mt-5 note note-code">
                                    $ cd.. | $ cd meinWorkspace | <span className="amber-text">In Workspace-Ordner wechseln z.B.: C:\projects</span><br />
                        $ mkdir meinProjektordner | <span className="amber-text">Projekt-Ordner anlegen (falls gewünscht)</span><br />
                        $ cd meinProjektordner | <span className="amber-text">In Projekt-Ordner wechseln</span><br />
                        $ git clone https://github.com/othello50/mdb-react-pro2.git | <span className="amber-text">Projekt clonen (git)</span></p>
                                  <p className="mt-5 mb-5 indigo-text">oder</p>
                                  <p className="mt-5 note note-code">$ yarn add https://github.com/othello50/mdb-react-pro2.git | <span className="amber-text">Projekt clonen (yarn></span></p>
                                </div>
                                <MDBAlert color="warning" className="mt-5 mb-5">
                                  <MDBIcon icon="exclamation-triangle" size="lg" className="mr-2" />
                                  <strong>Achtung:</strong> Es empfielt sich entweder yarn oder npm zu benutzen um Konflikte in der package.json zu vermeiden.
                                </MDBAlert>
                              </div>
                            </div>
                          </MDBCardBody>
                        </MDBCollapse>
                      </MDBCard>

                      <MDBCard className='mb-4'>
                        <MDBCollapseHeader
                          onClick={this.toggleCollapse('collapse19')}
                          className='p-0 z-depth-1'
                          tag='h4'
                          tagClassName='text-uppercase white-text mb-0 d-flex justify-content-start align-items-center'
                        >
                          <div
                            className='d-flex justify-content-center align-items-center mr-4'
                            style={{ backgroundColor: '#fff', minWidth: '100px' }}
                          >
                            <MDBIcon icon='play' size='2x' className='m-3 black-text' />
                          </div>
                        Projekt starten
                      </MDBCollapseHeader>

                        <MDBCollapse id='collapse19' isOpen={collapseID}>
                          <MDBCardBody className='rgba-black-light white-text z-depth-1'>
                            <div className='p-md-4 mb-0'>
                              <div className="list-group-flush">
                                <div className="list-group-item">
                                  <p className="mb-0">
                                    <MDBBtn floating size="2x" color="rws" className="pb-2"><MDBIcon fab icon="play-circle" /></MDBBtn>
                                    <span className="h4">
                                      <strong className="indigo-text">Projekt</strong>
                                      <small className="text-muted"> - starten</small>
                                    </span>
                                  </p>
                                  <p className="note note-rws-white"><strong>Note: </strong>
                      Das Projekt kann nun gestartet werden indem man folgendes in der Kommandozeile eingibt:</p>
                                  <p className="mt-5 note note-code">
                                    $ yarn start | $ cd npm start| <span className="amber-text">Projekt starten (yarn)</span><br /></p>
                                  <p className="mt-5 mb-r indigo-text">oder</p>
                                  <p className="mt-5 note note-code">$ npm start | <span className="amber-text">Projekt starten (npm)</span></p>
                                  <p className="mt-5 note note-rws-white"><strong>Note: </strong>
                      Nach erfolgreichem Starten wird folgendes in der Kommandozeile angezeigt und das Projekt kann über die angezeigten URL's aufgerufen werden. </p>
                                  <div className="mt-5 mb-5 note note-code">
                                    <p className="lime-text mb-3">Compiled successfully!</p>
                                    <p className="mb-2">You can now view your-app in the browser</p>
                                    <div className="ml-5">Local: http://localhost:3000</div>
                                    <div className="ml-5 mb-2">On Your Network: http://localhost:3000</div>
                                    <div>Note that the developement build ist not optimized.</div>
                                    <div>To create a production build use, <span className="blue-text">yarn build</span></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </MDBCardBody>
                        </MDBCollapse>
                      </MDBCard>
                      <MDBCard className='mb-4'>
                        <MDBCollapseHeader
                          onClick={this.toggleCollapse('collapse20')}
                          className='p-0 z-depth-1'
                          tag='h4'
                          tagClassName='text-uppercase white-text mb-0 d-flex justify-content-start align-items-center'
                        >
                          <div
                            className='d-flex justify-content-center align-items-center mr-4'
                            style={{ backgroundColor: '#fff', minWidth: '100px' }}
                          >
                            <MDBIcon icon='cogs' size='2x' className='m-3 black-text' />
                          </div>
                        Pre - Installation (optional)
                      </MDBCollapseHeader>

                        <MDBCollapse id='collapse20' isOpen={collapseID}>
                          <MDBCardBody className='rgba-black-light white-text z-depth-1'>
                            <div className='p-md-4 mb-0'>
                              <div className="list-group-flush">
                                <div className="list-group-item">
                                  <p className="mb-0">
                                    <MDBBtn floating size="2x" color="rws" className="pb-2"><MDBIcon icon="edit" /></MDBBtn>
                                    <span className="h4">
                                      <strong className="indigo-text">Projekt</strong>
                                      <small className="text-muted"> - updaten</small>
                                    </span>
                                  </p>
                                  <p className="note note-rws-white"><strong>Note: </strong>
                      Die Dependencies des Projekt können über folgende Eingaben in der Kommandozeile aktualisiert werden (Beispiel npm):</p>
                                  <p className="mt-5 note note-code">
                                    $ npm i npm-check-updates -g | <span className="amber-text">Check for Updates</span><br /></p>
                                  <p className="mt-5 note note-code">$ ncu -u | <span className="amber-text">Updates holen</span></p>
                                  <p className="mt-5 note note-code">$ npm install | <span className="amber-text">Updates installieren</span></p>
                                  <MDBAlert color="warning" className="mt-5 mb-5">
                                    <MDBIcon icon="exclamation-triangle" size="lg" className="mr-2" />
                                    <strong>Achtung:</strong> Bitte vorher Sicherung anlegen.
                                </MDBAlert>

                                </div>
                              </div>

                            </div>
                          </MDBCardBody>
                        </MDBCollapse>
                      </MDBCard>
                    </MDBContainer>
                  </MDBCol>
                </MDBRow>
              </div>
            </MDBCard>
          </SectionContainer>
        </MDBContainer>


      </Router>
    );
  }
}

export default SetupSetupProjectPage;
