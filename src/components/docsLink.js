import React from 'react';
import { MDBRow, MDBIcon, MDBCol } from 'mdbreact';

const DocsLink = ({ title, href }) => {
  return (
    <>
      <MDBRow className='align-items-center mt-5'>
        <MDBCol md='12'>
        <span className='grey-text' style={{ margin: '0px' }}>
          <strong className='font-weight-bold'>{title}</strong>
        </span>
        <a
          className='border white-text px-2 border-light rounded ml-2 peach-gradient'
          target='_blank'
          href={`${href}/?utm_source=DemoApp&utm_medium=MDBReactPro`}
          rel='noopener noreferrer'
        >
          <MDBIcon icon='graduation-cap' className='mr-2' />
          Docs
        </a>

        </MDBCol>
        
      </MDBRow>
      <hr className='mb-4' />
    </>
  );
};

export default DocsLink;
